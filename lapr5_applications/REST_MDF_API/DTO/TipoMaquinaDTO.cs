using REST_MDF_API;
using System.Collections.Generic;

namespace REST_MDF_API.DTO{
    public class TipoMaquinaDTO{
        public string designacao {get; set;}
        public List<OperacaoDTO> operacoes {get; set;}

        public TipoMaquinaDTO(){
            operacoes = new List<OperacaoDTO>();
        }

        public TipoMaquinaDTO(string designacao, List<OperacaoDTO> operacoes)
        {
            this.designacao = designacao;
            this.operacoes = operacoes;
        } 
    }
}