using REST_MDF_API.Model;

namespace REST_MDF_API.DTO
{
    public class OperacaoDTO
    {
        public string designacao { get; set; }
        public string ferramenta { get; set; }
        public float duracao { get; set; }

        public OperacaoDTO()
        {
            this.designacao = "";
            this.ferramenta = "";
            this.duracao = -1;
        }

        public OperacaoDTO(string designacao, string ferramenta, float duracao)
        {
            this.designacao = designacao;
            this.ferramenta = ferramenta;
            this.duracao = duracao;
        }
    }
}