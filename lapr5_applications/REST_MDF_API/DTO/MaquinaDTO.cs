using REST_MDF_API.Model;
using System.Collections.Generic;

namespace REST_MDF_API.DTO
{
    public class MaquinaDTO 
    {
        public string nome {get; set;}
        public TipoMaquinaDTO tipo {get; set;}

        public string estado {get;set;}
       

         public MaquinaDTO(){
        }

        public MaquinaDTO(string nome, TipoMaquinaDTO tipo)
        {
            this.nome = nome;
            this.tipo = tipo;
        }

        public MaquinaDTO(string nome, TipoMaquinaDTO tipo, string estado){
            this.nome = nome;
            this.tipo = tipo;
            this.estado = estado;
            
        }   
    }   
}