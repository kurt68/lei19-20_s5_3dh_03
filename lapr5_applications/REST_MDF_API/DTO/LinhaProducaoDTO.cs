using REST_MDF_API.Model;
using System.Collections.Generic;
namespace REST_MDF_API.DTO
{
    public class LinhaProducaoDTO 
    {
        public string designacao {get; set;}
        public LinkedList<MaquinaDTO> sequenciaMaquinas {get; set;}

        public LinhaProducaoDTO()
        {
        }

        public LinhaProducaoDTO(string designacao, LinkedList<MaquinaDTO> sequenciaMaquinas)
        {
            this.designacao = designacao;
            this.sequenciaMaquinas = sequenciaMaquinas;
        }

    }
}