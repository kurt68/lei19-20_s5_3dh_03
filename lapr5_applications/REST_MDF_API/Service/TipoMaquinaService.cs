using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using System.Collections.Generic;
using REST_MDF_API.Persistence;

namespace REST_MDF_API.Service
{

    public class TipoMaquinaService
    {

        private TipoMaquinaRepository repository;
        private mdfContext context;


        public TipoMaquinaService(mdfContext context)
        {
            this.context = context;
            this.repository = new TipoMaquinaRepository(context);
        }
        public TipoMaquinaDTO objToDTO(TipoMaquina o)
        {
            var dto = new TipoMaquinaDTO();
            dto.designacao = o.designacao.valueOf();
            foreach (TipoMaqOperacao item in o.tipoMaqOperacao)
            {
                item.operacao = new OperacaoService(context).findByID(item.operacaoID);
                dto.operacoes.Add(new OperacaoService(context).objToDTO(item.operacao));
            }
            return dto;
        }

        public TipoMaquina dtoToObj(TipoMaquinaDTO o)
        {
            var obj = new TipoMaquina();
            obj.designacao.setValue(o.designacao);
            if (findByDesignacao(o.designacao) != -1)
            {
                return obj = findByID(findByDesignacao(o.designacao));
            }
            foreach (OperacaoDTO item in o.operacoes)
            {
                TipoMaqOperacao maqOp = new TipoMaqOperacao();
                maqOp.operacao = new OperacaoService(context).dtoToObj(item);

                obj.tipoMaqOperacao.Add(maqOp);
            }
            return obj;
        }

        public TipoMaquina updateObj(TipoMaquina tp, TipoMaquinaDTO dto)
        {
            tp.designacao.setValue(dto.designacao);
            tp.tipoMaqOperacao.Clear();
            foreach (OperacaoDTO item in dto.operacoes)
            {
                TipoMaqOperacao maqOp = new TipoMaqOperacao();
                maqOp.operacao = new OperacaoService(context).dtoToObj(item);

                tp.tipoMaqOperacao.Add(maqOp);
            }
            return tp;
        }

        public List<TipoMaquinaDTO> objListToDTO(List<TipoMaquina> lista)
        {
            List<TipoMaquinaDTO> nova = new List<TipoMaquinaDTO>();
            foreach (TipoMaquina item in lista)
            {
                nova.Add(objToDTO(item));
            }
            return nova;
        }

        public TipoMaquina findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

        public long findByDesignacao(string tipoMaquina)
        {
            foreach (TipoMaquina item in this.repository.GetEntities())
            {
                if (item.designacao.valueOf().Equals(tipoMaquina))
                {
                    return item.Id;
                }
            }
            return -1;
        }

        public IEnumerable<string> getDescricoes() {
            return this.repository.GetDescricoes();
        }
    }
}
