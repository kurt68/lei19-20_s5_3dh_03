using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Persistence;
using System.Collections.Generic;
namespace REST_MDF_API.Service
{
    public class LinhaProducaoService
    {
        private LinhaProducaoRepository repository;
        private MaquinaService maquinaService;

        public LinhaProducaoService(mdfContext context)
        {
            this.repository = new LinhaProducaoRepository(context);
            this.maquinaService = new MaquinaService(context);
        }

        public LinhaProducaoDTO objToDTO(LinhaProducao o)
        {
            var dto = new LinhaProducaoDTO();
            dto.designacao = o.designacao.valueOf();
            LinkedList<MaquinaDTO> listadto = new LinkedList<MaquinaDTO>();
            foreach(Maquina m in o.sequenciaMaquinas)
            {
                listadto.AddLast(maquinaService.objToDTO(m));
            }
            dto.sequenciaMaquinas = listadto;
            return dto;
        }

        public LinhaProducao dtoToObj(LinhaProducaoDTO o)
        {
            var obj = new LinhaProducao();
            obj.designacao.setValue(o.designacao);
            obj.sequenciaMaquinas = maquinaService.dtoListToObj(o.sequenciaMaquinas);
            return obj;
        }

        public List<LinhaProducaoDTO> objListToDTO(List<LinhaProducao> lista)
        {
            List<LinhaProducaoDTO> nova = new List<LinhaProducaoDTO>();
            foreach (LinhaProducao item in lista)
            {
                nova.Add(this.objToDTO(item));
            }
            return nova;
        }

        public LinhaProducao updateObj(LinhaProducao o, LinhaProducaoDTO dto)
        {
            o.designacao.setValue(dto.designacao);
            o.sequenciaMaquinas = maquinaService.dtoListToObj(dto.sequenciaMaquinas);
            return o;
        }

        public LinhaProducao findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

    }
}