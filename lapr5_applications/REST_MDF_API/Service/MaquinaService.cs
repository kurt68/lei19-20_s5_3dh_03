using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Persistence;
using System.Collections.Generic;
namespace REST_MDF_API.Service
{
    public class MaquinaService
    {
        private MaquinaRepository repository;
        private TipoMaquinaService tipoMaquinaService;

        public MaquinaService(mdfContext context)
        {
            this.repository = new MaquinaRepository(context);
            this.tipoMaquinaService = new TipoMaquinaService(context);
        }

        public MaquinaDTO objToDTO(Maquina o)
        {
            var dto = new MaquinaDTO();
            dto.nome = o.nome.valueOf();
            dto.tipo = tipoMaquinaService.objToDTO(o.tipo);
            dto.estado = o.estado;
            return dto;
        }

        public Maquina dtoToObj(MaquinaDTO o)
        {
            var obj = new Maquina();
            if(findByDesignacao(o.nome) != -1){
                obj = findByID(findByDesignacao(o.nome));
                return obj;
            }
            obj.nome.setValue(o.nome);
            obj.tipo = tipoMaquinaService.dtoToObj(o.tipo);
            o.estado = obj.estado;
            return obj;
        }

        public List<MaquinaDTO> objListToDTO(List<Maquina> lista)
        {
            List<MaquinaDTO> nova = new List<MaquinaDTO>();
            foreach (Maquina item in lista)
            {
                nova.Add(this.objToDTO(item));
            }
            return nova;
        }

        public LinkedList<Maquina> dtoListToObj(LinkedList<MaquinaDTO> lista)
        {
            LinkedList<Maquina> nova = new LinkedList<Maquina>();
            foreach (MaquinaDTO itm in lista)
            {
                nova.AddLast(this.dtoToObj(itm));
            }
            return nova;
        }
        public Maquina updateObj(Maquina maquina, MaquinaDTO dto)
        {
            maquina.nome.setValue(dto.nome);
            maquina.tipo = tipoMaquinaService.dtoToObj(dto.tipo);
            return maquina;
        }

        public Maquina findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

        public Maquina alterarEstado (Maquina maquina, string estado) {
            maquina.estado = estado;
            return maquina;
        }

        public long findByDesignacao(string maquina)
        {
            foreach (Maquina item in this.repository.GetEntities())
            {
                if (item.nome.valueOf().Equals(maquina))
                {
                    return item.Id;
                }
            }
            return -1;
        }

        public IEnumerable<string> getDescricoes() {
            return this.repository.GetDescricoes();
        }
    }
}