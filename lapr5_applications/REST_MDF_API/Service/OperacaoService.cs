using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Persistence;
using System.Collections.Generic;

namespace REST_MDF_API.Service{

    public class OperacaoService{

        private OperacaoRepository repository;

        public OperacaoService(mdfContext context){
            
            this.repository = new OperacaoRepository(context);
        }
        public OperacaoDTO objToDTO (Operacao o)
        {
            var dto = new OperacaoDTO();
            dto.designacao = o.designacao.valueOf();
            dto.ferramenta = o.ferramenta.valueOf();
            dto.duracao = o.duracao.valueOf();
            return dto;
        }

        public Operacao dtoToObj(OperacaoDTO o)
        {
            if (findByDesignacao(o.designacao, o.ferramenta) != -1){
                return findByID(findByDesignacao(o.designacao, o.ferramenta));
            }
            var obj = new Operacao();
            obj.designacao.setValue(o.designacao);
            obj.ferramenta.setValue(o.ferramenta);
            obj.duracao.setValue(o.duracao);
            return obj;
        }

        public List<OperacaoDTO> objListToDTO(List<Operacao> lista)
        {
            List<OperacaoDTO> nova = new List<OperacaoDTO>();
            foreach (Operacao item in lista)
            {
                nova.Add(objToDTO(item));
            }
            return nova;
        }

        public Operacao findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

        public Operacao updateObj(Operacao operacao, OperacaoDTO dto)
        {
            operacao.designacao.setValue(dto.designacao);
            operacao.ferramenta.setValue(dto.ferramenta);
            operacao.duracao.setValue(dto.duracao);
            return operacao;
        }

        public long findByDesignacao(string operacao, string ferramenta){
            foreach (Operacao item in this.repository.GetEntities())
            {
                if (item.designacao.valueOf().Equals(operacao) & item.ferramenta.valueOf().Equals(ferramenta)){
                    return item.Id;
                }
            }
            return -1;
        }

        public IEnumerable<string> getDescricoes() {
            return this.repository.GetDescricoes();
        }
    }
}
