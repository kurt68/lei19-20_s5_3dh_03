using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Service;
namespace REST_MDF_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinhaProducaoController : ControllerBase
    {
        private readonly mdfContext _context;
        private LinhaProducaoService service;

        public LinhaProducaoController(mdfContext context)
        {
            _context = context;
            service = new LinhaProducaoService(context);
        }

        // GET: api/LinhaProducao
        [HttpGet]
        public async Task<IActionResult> GetTodoItems()
        {
            var list = await _context.LinhaProducaoItems.Include(lp => lp.sequenciaMaquinas).ThenInclude(m => m.tipo).ThenInclude(tp => tp.tipoMaqOperacao).ToListAsync();
            return Ok(service.objListToDTO(list));
        }

        // GET: api/LinhaProducao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItem([FromRoute]long id)
        {
            LinhaProducao todoItem = await _context.LinhaProducaoItems.Include(lp => lp.sequenciaMaquinas).ThenInclude(m => m.tipo).ThenInclude(tp => tp.tipoMaqOperacao).Where(x => x.Id == id).SingleAsync();
            if (todoItem == null)
            {
                return NotFound();
            }

            return Ok(service.objToDTO(todoItem));
        }

        // POST: api/LinhaProducao
        [HttpPost]
        public async Task<IActionResult> PostTodoItem(LinhaProducaoDTO item){
            _context.LinhaProducaoItems.Add(service.dtoToObj(item));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTodoItem), new { id = item }, item);
        }

        // PUT: api/LinhaProducao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, LinhaProducaoDTO item){
            LinhaProducao lp = await _context.LinhaProducaoItems.Include(x => x.sequenciaMaquinas).ThenInclude(m => m.tipo).ThenInclude(tp => tp.tipoMaqOperacao).Where(x => x.Id == id).SingleAsync();
            
            if (id != lp.Id){
                return BadRequest();
            }
            
            service.updateObj(lp,item);
            _context.Entry(lp).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(lp);
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute]long id)
        {
            LinhaProducao o = service.findByID(id);
            if (id != o.Id)
            {
                return BadRequest();
            }   
            _context.Remove(o);
            await _context.SaveChangesAsync();
            return Ok();
        }

    }
}