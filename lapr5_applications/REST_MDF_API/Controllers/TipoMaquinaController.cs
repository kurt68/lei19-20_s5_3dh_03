using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Service;

namespace REST_MDF_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoMaquinaController : ControllerBase
    {
        private readonly mdfContext _context;
        private TipoMaquinaService service;

        public TipoMaquinaController(mdfContext context)
        {
            _context = context;
            service = new TipoMaquinaService(context);
        }

        // GET: api/TipoMaquina
        [HttpGet("getDesignacoes")]
        public IEnumerable<string> getDesignacoes()
        {
            return this.service.getDescricoes();
        }

        // GET: api/TipoMaquina
        [HttpGet]
        public async Task<List<TipoMaquinaDTO>> GetTodoItems()
        {
            var tipoMaquina = await _context.TipoMaquinaItems.Include(tm => tm.tipoMaqOperacao).ThenInclude(m => m.operacao).ToListAsync();

            var listaDTO = new List<TipoMaquinaDTO>();
            foreach (var item in tipoMaquina)
            {
                listaDTO.Add(service.objToDTO(item));
            }
            return listaDTO;
        }

        // GET: api/TipoMaquina/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItem([FromRoute]long id)
        {
            var tipoMaquina = await _context.TipoMaquinaItems.Include(tm => tm.tipoMaqOperacao).ThenInclude(m => m.operacao).Where(a => a.Id == id).SingleAsync();

            if (tipoMaquina == null)
            {
                return NotFound();
            }

            return Ok(service.objToDTO(tipoMaquina));
        }

        // TipoMaquina/{id}/Maquinas  GET
        [HttpGet("{designacao}/Maquinas")]
        public async Task<IEnumerable<MaquinaDTO>> GetPartes([FromRoute] string designacao)
        {
            TipoMaquina tp = await _context.TipoMaquinaItems.Include(tm => tm.tipoMaqOperacao).ThenInclude(m => m.operacao).Where(a => a.designacao.valueOf().Equals(designacao)).SingleAsync();
            List<Maquina> maquinas= await _context.MaquinaItems.Include(m => m.tipo).ToListAsync();
            var listaMaquinas = new List<MaquinaDTO>();

            foreach (Maquina m in maquinas)
            {
                if(m.tipo.Id.Equals(tp.Id))
                {
                    listaMaquinas.Add(new MaquinaService(_context).objToDTO(m));
                }
            }
            return listaMaquinas;
        }

        // POST: api/TipoMaquina
        [HttpPost]
        public async Task<IActionResult> PostTodoItem(TipoMaquinaDTO item)
        {
            _context.TipoMaquinaItems.Add(service.dtoToObj(item));
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTodoItem), new { id = item }, item);
        }

        // PUT: api/TipoMaquina/5
       /*/ [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem([FromRoute]long id, TipoMaquinaDTO item)
        {
            TipoMaquina tp = await _context.TipoMaquinaItems.Include(tm => tm.tipoMaqOperacao).ThenInclude(m => m.operacao).Where(a => a.Id == id).SingleAsync();
            if (id != tp.Id)
            {
                return BadRequest();

            }
            service.updateObj(tp, item);    
            _context.Entry(tp).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok();
        }
        */

         // PUT: api/TipoMaquina/5
        [HttpPut("{designacao}")]
        public async Task<IActionResult> PutTodoItem([FromRoute]string designacao, TipoMaquinaDTO item)
        {
            TipoMaquina tp = await _context.TipoMaquinaItems.Include(tm => tm.tipoMaqOperacao).ThenInclude(m => m.operacao).Where(a => a.designacao.valueOf().Equals(designacao)).SingleAsync();
            if (!designacao.Equals(tp.designacao.valueOf()))
            {
                return BadRequest();
                
            }
            service.updateObj(tp, item);    
            _context.Entry(tp).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute]long id)
        {
            TipoMaquina tp = service.findByID(id);
            if (id != tp.Id)
            {
                return BadRequest();
            }   
            _context.Remove(tp);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}