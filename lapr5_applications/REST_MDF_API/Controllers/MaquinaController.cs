using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Service;

namespace REST_MDF_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaquinaController : ControllerBase
    {
        private readonly mdfContext _context;
        private MaquinaService service;

        public MaquinaController(mdfContext context)
        {
            _context = context;
            service = new MaquinaService(context);

        }

        // GET: api/Maquina
       /*  [HttpGet("getDesignacoes")]
        public IEnumerable<string> getDesignacoes()
        {
            return this.service.getDescricoes();
        }
        */

        // GET: api/Maquina
        [HttpGet]
        public async Task<IActionResult> GetTodoItems()
        {
            var maquina = await _context.MaquinaItems.Include(m => m.tipo).ThenInclude(tm => tm.tipoMaqOperacao).ToListAsync();
            return Ok(service.objListToDTO(maquina));
        }

        // GET: api/Maquina/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItem([FromRoute]long id)
        {
            Maquina todoItem = await _context.MaquinaItems.Include(m => m.tipo).ThenInclude(tm => tm.tipoMaqOperacao).Where(a => a.Id == id).SingleAsync();
            if (todoItem == null)
            {
                return NotFound();
            }

            return Ok(service.objToDTO(todoItem));
        }

        // POST: api/Maquina
        [HttpPost]
        public async Task<IActionResult> PostTodoItem(MaquinaDTO item)
        {
            Maquina m = service.dtoToObj(item);
            _context.MaquinaItems.Add(m);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTodoItem), new { id = m.Id }, item);
        }

        // PUT: api/Maquina/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem([FromRoute]long id, MaquinaDTO item)
        {
            Maquina m = service.findByID(id);
            if (id != m.Id)
            {
                return BadRequest();
            }
            service.updateObj(m,item);
            _context.Entry(m).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(m);
        }

        // PUT: api/Maquina/5
        [HttpPut("{nome}/nome")]
        public async Task<IActionResult> PutTodoItemNome([FromRoute]string nome, MaquinaDTO item)
        {
            long idM = service.findByDesignacao(nome);
            if (idM == -1)
            {
                return BadRequest();
            }
            Maquina m = service.findByID(idM);
            service.updateObj(m,item);
            _context.Entry(m).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(m);
        }

         [HttpPut("{nome}/estado")]
        public async Task<IActionResult> alterarEstado([FromRoute]string nome, MaquinaDTO dto)
        {
            long idM = service.findByDesignacao(dto.nome);
            if (idM == -1)
            {
                return BadRequest();
            }
            Maquina m = service.findByID(idM);
            service.alterarEstado(m,dto.estado);
            _context.Entry(m).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(m);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute]long id)
        {
            Maquina o = service.findByID(id);
            if (id != o.Id)
            {
                return BadRequest();
            }   
            _context.Remove(o);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}