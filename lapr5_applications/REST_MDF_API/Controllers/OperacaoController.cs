using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using REST_MDF_API.Model;
using REST_MDF_API.DTO;
using REST_MDF_API.Service;

namespace REST_MDF_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperacaoController : ControllerBase
    {
        private readonly mdfContext _context;
        private OperacaoService service;

        public OperacaoController(mdfContext context)
        {
            _context = context;
            service = new OperacaoService(context);

        }

        // GET: api/Operacao
        [HttpGet]
        public async Task<IActionResult> GetTodoItems()
        {
            List<Operacao> list = await _context.OperacaoItems.ToListAsync();
            return Ok(service.objListToDTO(list));
        }

        // GET: api/Operacao
        [HttpGet("{op}")]
        public async Task<IActionResult> getDesignacoes([FromRoute] string op)
        {
            string []param = op.Trim().Split(",");
            long idOp = this.service.findByDesignacao(param[0], param[1]);
            var todoItem = await _context.OperacaoItems.FindAsync(idOp);
            if (todoItem == null)
            {
                return NotFound();
            }
            return Ok(service.findByID(idOp).Id);
        }

        // GET: api/Operacao/id
        [HttpGet("{id}/id")]
        public async Task<IActionResult> GetTodoItem([FromRoute]long id)
        {
            Operacao todoItem = await _context.OperacaoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }

            return Ok(service.objToDTO(todoItem));
        }

        // POST: api/Operacao
        [HttpPost]
        public async Task<IActionResult> PostTodoItem(OperacaoDTO item)
        {
            Operacao o = service.dtoToObj(item);
            _context.OperacaoItems.Add(o);
            await _context.SaveChangesAsync();

            return Ok();
        }

        // PUT: api/Operacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem([FromRoute]long id, OperacaoDTO item)
        {
            Operacao o = service.findByID(id);
            if (id != o.Id)
            {
                return BadRequest();
            }
            service.updateObj(o,item);
            _context.Entry(o).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(o);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute]long id)
        {
            Operacao o = service.findByID(id);
            if (id != o.Id)
            {
                return BadRequest();
            }   
            _context.Remove(o);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}