using System;
using System.Collections.Generic;
using REST_MDF_API.Model;
using System.Linq;
namespace REST_MDF_API.Persistence
{
    public class MaquinaRepository : IRepository<Maquina>
    {
        private mdfContext context;

        public MaquinaRepository(mdfContext context)
        {
            this.context = context;
        }

        public void DeleteEntity(long TId)
        {
            Maquina tm = context.MaquinaItems.Find(TId);
            context.MaquinaItems.Remove(tm);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Maquina> GetEntities()
        {
            return context.MaquinaItems.ToList();
        }

        public Maquina GetEntityByID(long TId)
        {
            return context.MaquinaItems.Find(TId);
        }

        public void InsertEntity(Maquina t)
        {
            context.MaquinaItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(Maquina t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public IEnumerable<string> GetDescricoes() {
            return context.MaquinaItems.Select(o=> o.nome.designacao).ToList();
        }

    }
}
