using System;
using System.Collections.Generic;
using REST_MDF_API.Model;
using System.Linq;
namespace REST_MDF_API.Persistence
{
    public class TipoMaquinaRepository : IRepository<TipoMaquina>
    {
        private mdfContext context;

        public TipoMaquinaRepository(mdfContext context)
        {
            this.context = context;
        }

        public void DeleteEntity(long TId)
        {
            TipoMaquina tm = context.TipoMaquinaItems.Find(TId);
            context.TipoMaquinaItems.Remove(tm);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TipoMaquina> GetEntities()
        {
            return context.TipoMaquinaItems.ToList();
        }

        public TipoMaquina GetEntityByID(long TId)
        {
            return context.TipoMaquinaItems.Find(TId);
        }

        public void InsertEntity(TipoMaquina t)
        {
            context.TipoMaquinaItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(TipoMaquina t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public IEnumerable<string> GetDescricoes() {
            return context.TipoMaquinaItems.Select(o=> o.designacao.designacao).ToList();
        }
    }
}