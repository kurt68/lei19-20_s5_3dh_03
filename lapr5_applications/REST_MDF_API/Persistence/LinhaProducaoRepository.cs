using System;
using System.Collections.Generic;
using REST_MDF_API.Model;
using System.Linq;
namespace REST_MDF_API.Persistence
{
    public class LinhaProducaoRepository : IRepository<LinhaProducao>
    {
        private mdfContext context;

        public LinhaProducaoRepository(mdfContext context)
        {
            this.context = context;
        }


        public void DeleteEntity(long TId)
        {
            LinhaProducao tm = context.LinhaProducaoItems.Find(TId);
            context.LinhaProducaoItems.Remove(tm);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LinhaProducao> GetEntities()
        {
            return context.LinhaProducaoItems.ToList();
        }

        public LinhaProducao GetEntityByID(long TId)
        {
            return context.LinhaProducaoItems.Find(TId);
        }

        public void InsertEntity(LinhaProducao t)
        {
            context.LinhaProducaoItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(LinhaProducao t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }
    }
}


