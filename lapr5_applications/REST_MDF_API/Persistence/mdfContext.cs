using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using REST_MDF_API.Model;

namespace REST_MDF_API
{
    public class mdfContext : DbContext 
    {
        public mdfContext(DbContextOptions<mdfContext> options) : base(options) 
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Operacao>().OwnsOne(op => op.ferramenta);
            modelBuilder.Entity<Operacao>().OwnsOne(op => op.designacao);
            modelBuilder.Entity<Operacao>().OwnsOne(op=> op.duracao);
            modelBuilder.Entity<Maquina>().OwnsOne(m => m.nome).HasIndex(d => d.designacao).IsUnique();
            modelBuilder.Entity<TipoMaquina>().OwnsOne(tm => tm.designacao).HasIndex(d => d.designacao).IsUnique();
            modelBuilder.Entity<LinhaProducao>().OwnsOne(lp => lp.designacao).HasIndex(d => d.designacao).IsUnique();
            
        }


        public DbSet<Maquina> MaquinaItems { get; set; }
        public DbSet<Operacao> OperacaoItems { get; set; }
        public DbSet<TipoMaquina> TipoMaquinaItems { get; set; }
        public DbSet<LinhaProducao> LinhaProducaoItems { get; set; }
        public DbSet<TipoMaqOperacao> TipoMaqOperacaoItems { get; set; }
        

    }
}