using System;
using System.Collections.Generic;
using REST_MDF_API.Model;
using System.Linq;
namespace REST_MDF_API.Persistence
{
    public class OperacaoRepository : IRepository<Operacao>
    {
        private mdfContext context;

        public OperacaoRepository(mdfContext context)
        {
            this.context = context;
        }

        public void DeleteEntity(long TId)
        {
            Operacao op = context.OperacaoItems.Find(TId);
            context.OperacaoItems.Remove(op);
        }

        public IEnumerable<Operacao> GetEntities()
        {
            return context.OperacaoItems.ToList();
        }

        public Operacao GetEntityByID(long TId)
        {
            return context.OperacaoItems.Find(TId);
        }

        public void InsertEntity(Operacao t)
        {
            context.OperacaoItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(Operacao t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public List<string> GetDescricoes() {
            List<string> list = new List<string>();
            foreach( Operacao o in context.OperacaoItems.ToList()) {
                string op = o.designacao.valueOf() + " " + o.ferramenta.valueOf();
                list.Add(op);
            }
            return list;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~OperacaoRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}