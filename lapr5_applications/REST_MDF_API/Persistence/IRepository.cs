using System;
using System.Collections.Generic;
namespace REST_MDF_API
{
    public interface IRepository<T> : IDisposable
    {
        IEnumerable<T> GetEntities();
        T GetEntityByID(long TId);
        void InsertEntity(T t);
        void DeleteEntity(long TId);
        void UpdateEntity(T t);
        void save();
    }
}