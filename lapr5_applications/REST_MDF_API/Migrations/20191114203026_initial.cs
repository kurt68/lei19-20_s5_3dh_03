﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace REST_MDF_API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LinhaProducaoItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    designacao_designacao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinhaProducaoItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OperacaoItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    designacao_designacao = table.Column<string>(nullable: true),
                    ferramenta_nome = table.Column<string>(nullable: true),
                    duracao_duracao = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperacaoItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoMaquinaItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    designacao_designacao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMaquinaItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MaquinaItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nome_designacao = table.Column<string>(nullable: true),
                    tipoId = table.Column<long>(nullable: true),
                    LinhaProducaoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaquinaItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaquinaItems_LinhaProducaoItems_LinhaProducaoId",
                        column: x => x.LinhaProducaoId,
                        principalTable: "LinhaProducaoItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MaquinaItems_TipoMaquinaItems_tipoId",
                        column: x => x.tipoId,
                        principalTable: "TipoMaquinaItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TipoMaqOperacaoItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    tipoMaquinaId = table.Column<long>(nullable: false),
                    operacaoID = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoMaqOperacaoItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TipoMaqOperacaoItems_OperacaoItems_operacaoID",
                        column: x => x.operacaoID,
                        principalTable: "OperacaoItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TipoMaqOperacaoItems_TipoMaquinaItems_tipoMaquinaId",
                        column: x => x.tipoMaquinaId,
                        principalTable: "TipoMaquinaItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LinhaProducaoItems_designacao_designacao",
                table: "LinhaProducaoItems",
                column: "designacao_designacao",
                unique: true,
                filter: "[designacao_designacao] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MaquinaItems_LinhaProducaoId",
                table: "MaquinaItems",
                column: "LinhaProducaoId");

            migrationBuilder.CreateIndex(
                name: "IX_MaquinaItems_tipoId",
                table: "MaquinaItems",
                column: "tipoId");

            migrationBuilder.CreateIndex(
                name: "IX_MaquinaItems_nome_designacao",
                table: "MaquinaItems",
                column: "nome_designacao",
                unique: true,
                filter: "[nome_designacao] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TipoMaqOperacaoItems_operacaoID",
                table: "TipoMaqOperacaoItems",
                column: "operacaoID");

            migrationBuilder.CreateIndex(
                name: "IX_TipoMaqOperacaoItems_tipoMaquinaId",
                table: "TipoMaqOperacaoItems",
                column: "tipoMaquinaId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoMaquinaItems_designacao_designacao",
                table: "TipoMaquinaItems",
                column: "designacao_designacao",
                unique: true,
                filter: "[designacao_designacao] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaquinaItems");

            migrationBuilder.DropTable(
                name: "TipoMaqOperacaoItems");

            migrationBuilder.DropTable(
                name: "LinhaProducaoItems");

            migrationBuilder.DropTable(
                name: "OperacaoItems");

            migrationBuilder.DropTable(
                name: "TipoMaquinaItems");
        }
    }
}
