﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace REST_MDF_API.Migrations
{
    public partial class lastOne : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "estado",
                table: "MaquinaItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "estado",
                table: "MaquinaItems");
        }
    }
}
