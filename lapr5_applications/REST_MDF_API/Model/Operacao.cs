using System.Collections.Generic;

namespace REST_MDF_API.Model
{
    public class Operacao
    {
        public long Id {get; set;}

        public Designacao designacao {get; set;} 
        public Ferramenta ferramenta {get; set;}
        public List<TipoMaqOperacao> tipoMaqOperacao {get; set;}
        public Duracao duracao {get; set;}
        
        public Operacao(){
            this.designacao = new Designacao();
            this.ferramenta = new Ferramenta();
            this.duracao = new Duracao();
        }

        public Operacao(Designacao designacao, Ferramenta ferramenta, List<TipoMaqOperacao> tipoMaqOperacao, Duracao duracao) {
            this.designacao = designacao;
            this.ferramenta = ferramenta;
            this.tipoMaqOperacao = tipoMaqOperacao;
            this.duracao = duracao;
        }

    }
}