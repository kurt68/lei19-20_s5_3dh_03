using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace REST_MDF_API.Model
{
    public class TipoMaqOperacao
    {

        public long Id {get; set;}
        public TipoMaquina tipoMaquina {get; set;}
        [ForeignKey("TipoMaquinaId")]
        public long tipoMaquinaId {get; set;}
        public Operacao operacao {get; set;}
        [ForeignKey("OperacaoId")]
        public long operacaoID {get; set;}

        public TipoMaqOperacao() {
        }
        
        public TipoMaqOperacao(TipoMaquina tipoMaquina, Operacao operacao) {
            this.tipoMaquina = tipoMaquina;
            this.operacao = operacao;
        }
    }
}