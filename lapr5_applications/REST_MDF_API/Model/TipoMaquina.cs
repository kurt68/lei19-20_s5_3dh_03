using System.Collections.Generic;

namespace REST_MDF_API.Model
{
    public class TipoMaquina
    {

        public long Id {get; set;}
        public Designacao designacao {get; set;}
        public List<TipoMaqOperacao> tipoMaqOperacao {get; set;}

        public TipoMaquina() {
            this.designacao = new Designacao();
            this.tipoMaqOperacao = new List<TipoMaqOperacao>();
        }

        public TipoMaquina(Designacao designacao, List<TipoMaqOperacao> tipoMaqOperacao) {
            this.designacao = designacao;
            this.tipoMaqOperacao = tipoMaqOperacao;
        } 
    }
}