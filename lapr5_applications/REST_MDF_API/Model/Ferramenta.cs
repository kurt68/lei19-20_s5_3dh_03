using System.Collections.Generic;

namespace REST_MDF_API.Model
{
    public class Ferramenta : ValueObject
    {
        public string nome {get; set;}

        public Ferramenta(){}

        public void setValue(string value)
        {
            this.nome = value;
        }

        public string valueOf()
        {
            return this.nome;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new System.NotImplementedException();
        }
    }
}