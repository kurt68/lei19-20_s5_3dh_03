using System.Collections.Generic;
namespace REST_MDF_API.Model
{

    public class Duracao : ValueObject
    {

        public float duracao { get; set; }

        public Duracao()
        {
        }

        public void setValue(float value)
        {
            this.duracao = value;
        }

        public float valueOf()
        {
            return this.duracao;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new System.NotImplementedException();
        }

    }
}