namespace REST_MDF_API.Model
{
    public class Maquina
    {

        public long Id {get; set;}
        public Designacao nome {get; set;}
        public TipoMaquina tipo {get; set;}
        public string estado {get;set;}
        

        public Maquina() {
            nome = new Designacao();
            tipo = new TipoMaquina();
        }

        public Maquina(Designacao nome, TipoMaquina tipo) {
            this.nome = nome;
            this.tipo = tipo;
        }

         public Maquina(Designacao nome, TipoMaquina tipo, string estado) {
            this.nome = nome;
            this.tipo = tipo;
            this.estado = estado;
        }
    }
}