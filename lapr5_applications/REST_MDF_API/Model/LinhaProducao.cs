using System.Collections.Generic;

namespace REST_MDF_API.Model
{
    public class LinhaProducao 
    {

        public long Id {get; set;}
        public Designacao designacao {get; set;}
        public LinkedList<Maquina> sequenciaMaquinas {get; set;}

        public LinhaProducao() {
            designacao = new Designacao();
            sequenciaMaquinas = new LinkedList<Maquina>();
        }

        public LinhaProducao(Designacao designacao, LinkedList<Maquina> sequenciaMaquinas) {
            this.sequenciaMaquinas = sequenciaMaquinas;
        }
      
    }
}