namespace REST_MDP_API.DTO
{
    public class OperacaoDTO
    {
        public long id { get; set; }
        public string designacao { get; set; }
        public string ferramenta { get; set; }
        public float duracao { get; set; }

        public OperacaoDTO()
        {
            this.id = -1;
            this.designacao = "";
            this.ferramenta = "";
            this.duracao = -1;
        }

        public OperacaoDTO(long id, string designacao, string ferramenta, float duracao)
        {
            this.id = id;
            this.designacao = designacao;
            this.ferramenta = ferramenta;
            this.duracao = duracao;
        }
    }
}