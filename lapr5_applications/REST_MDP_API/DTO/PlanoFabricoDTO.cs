using System.Collections.Generic;
namespace REST_MDP_API.DTO
{
    public class PlanoFabricoDTO
    {
        public long id { get; set; }
        public LinkedList<OperacaoDTO> operacoes { get; set; }
        public PlanoFabricoDTO()
        {
            this.operacoes = new LinkedList<OperacaoDTO>();
        }

        public PlanoFabricoDTO(LinkedList<OperacaoDTO> operacoes, long id)
        {
            this.id = id;
            this.operacoes = operacoes;
        }


    }
}