namespace REST_MDP_API.DTO
{
    public class ProdutoDTO
    {
        public string designacao {get; set;}
        public PlanoFabricoDTO planoFabrico {get; set;}
        
        public ProdutoDTO()
        {
        }

        public ProdutoDTO(string designacao, PlanoFabricoDTO planoFabrico)
        {
            this.designacao = designacao;
            this.planoFabrico = planoFabrico;
        }
    }
}