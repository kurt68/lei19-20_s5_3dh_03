﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace REST_MDP_API.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlanoFabricoItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanoFabricoItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IdOperacoesItems",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    idOperacao = table.Column<long>(nullable: false),
                    planoFabricoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdOperacoesItems", x => x.id);
                    table.ForeignKey(
                        name: "FK_IdOperacoesItems_PlanoFabricoItems_planoFabricoId",
                        column: x => x.planoFabricoId,
                        principalTable: "PlanoFabricoItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    designacao_designacao = table.Column<string>(nullable: true),
                    planoFabricoId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProdutoItems_PlanoFabricoItems_planoFabricoId",
                        column: x => x.planoFabricoId,
                        principalTable: "PlanoFabricoItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IdOperacoesItems_planoFabricoId",
                table: "IdOperacoesItems",
                column: "planoFabricoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoItems_designacao_designacao",
                table: "ProdutoItems",
                column: "designacao_designacao",
                unique: true,
                filter: "[designacao_designacao] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoItems_planoFabricoId",
                table: "ProdutoItems",
                column: "planoFabricoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IdOperacoesItems");

            migrationBuilder.DropTable(
                name: "ProdutoItems");

            migrationBuilder.DropTable(
                name: "PlanoFabricoItems");
        }
    }
}
