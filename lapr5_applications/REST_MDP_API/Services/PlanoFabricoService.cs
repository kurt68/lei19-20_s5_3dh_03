using REST_MDP_API.Model;
using REST_MDP_API.DTO;
using REST_MDP_API.Persistence;
using System.Collections.Generic;
namespace REST_MDP_API.Service
{
    public class PlanoFabricoService
    {

        private PlanoFabricoRepository repository;

        public PlanoFabricoService(mdpContext context)
        {
            this.repository = new PlanoFabricoRepository(context);
        }

        public PlanoFabricoDTO objToDTO(PlanoFabrico p)
        {
            var dto = new PlanoFabricoDTO();
            dto.id = p.Id;
            foreach (var item in p.operacoes)
            {
                OperacaoDTO idOp = new OperacaoDTO();
                idOp.id = item.id;
                dto.operacoes.AddLast(idOp);
            }
            return dto;
        }

        public PlanoFabrico dtoToObj(PlanoFabricoDTO p)
        {
            var obj = new PlanoFabrico();

            foreach (var item in p.operacoes)
            {
                IdOperacao operacao = new IdOperacao();
                operacao.setValue(item.id);
                obj.operacoes.AddLast(operacao);
            }
            return obj;
        }



        public List<PlanoFabricoDTO> objListToDTO(List<PlanoFabrico> lista)
        {
            List<PlanoFabricoDTO> nova = new List<PlanoFabricoDTO>();
            foreach (PlanoFabrico p in lista)
            {
                nova.Add(this.objToDTO(p));
            }
            return nova;
        }

        public List<PlanoFabrico> DTOListToObj(List<PlanoFabricoDTO> lista)
        {
            List<PlanoFabrico> nova = new List<PlanoFabrico>();
            foreach (PlanoFabricoDTO p in lista)
            {
                nova.Add(this.dtoToObj(p));
            }
            return nova;
        }
        public PlanoFabrico findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

    }
}