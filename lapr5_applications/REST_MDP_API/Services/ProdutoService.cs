using REST_MDP_API.Model;
using REST_MDP_API.DTO;
using REST_MDP_API.Persistence;
using System.Collections.Generic;
namespace REST_MDP_API.Service
{
    public class ProdutoService
    {
        private ProdutoRepository repository;
        private PlanoFabricoService planoFabricoService;

        public ProdutoService(mdpContext context)
        {
            this.repository = new ProdutoRepository(context);
            this.planoFabricoService = new PlanoFabricoService(context);
        }

        public ProdutoDTO objToDTO(Produto p)
        {
            var dto = new ProdutoDTO();
            dto.designacao = p.designacao.valueOf();
            dto.planoFabrico = this.planoFabricoService.objToDTO(p.planoFabrico);
            return dto;
        }

        public Produto DTOtoObj(ProdutoDTO p)
        {
            var obj = new Produto();
            obj.designacao.setValue(p.designacao);
            obj.planoFabrico = this.planoFabricoService.dtoToObj(p.planoFabrico);
            return obj;
        }

        public List<ProdutoDTO> objListToDTO(List<Produto> lista)
        {
            List<ProdutoDTO> nova = new List<ProdutoDTO>();
            foreach (Produto p in lista)
            {
                nova.Add(this.objToDTO(p));

            }
            return nova;
        }

        public List<Produto> dtoListToObj(List<ProdutoDTO> lista)
        {
            List<Produto> nova = new List<Produto>();
            foreach (ProdutoDTO p in lista)
            {
                nova.Add(this.DTOtoObj(p));

            }
            return nova;
        }

        public Produto updateObj(Produto produto, ProdutoDTO dto)
        {
            produto.designacao.setValue(dto.designacao);
            produto.planoFabrico = planoFabricoService.dtoToObj(dto.planoFabrico);
            return produto;
        }

        public Produto findByID(long Id)
        {
            return this.repository.GetEntityByID(Id);
        }

        public Produto findByNome(string nome){
            foreach (Produto item in this.repository.GetEntities())
            {
                if(item.designacao.valueOf().Equals(nome)){
                    return item;
                }
            }
            return null;
        }

        public IEnumerable<string> getDescricoes() {
            return this.repository.GetDescricoes();
        }

    }
}