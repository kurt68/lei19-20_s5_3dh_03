using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using REST_MDP_API.Model;

namespace REST_MDP_API
{
    public class mdpContext : DbContext 
    {
        public mdpContext(DbContextOptions<mdpContext> options) : base(options) 
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Produto>().OwnsOne(p => p.designacao).HasIndex(d => d.designacao).IsUnique();
        }


        public DbSet<Produto> ProdutoItems { get; set; }
        public DbSet<PlanoFabrico> PlanoFabricoItems { get; set; }        
        public DbSet<IdOperacao> IdOperacoesItems { get; set; }
    }
}