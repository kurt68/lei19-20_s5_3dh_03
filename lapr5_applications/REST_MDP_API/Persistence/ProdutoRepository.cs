using System;
using System.Collections.Generic;
using REST_MDP_API.Model;
using System.Linq;
namespace REST_MDP_API.Persistence
{
    public class ProdutoRepository : IRepository<Produto>
    {
        public mdpContext context;

        public ProdutoRepository(mdpContext context)
        {
            this.context = context;
        }

        public void DeleteEntity(long TId)
        {
            Produto p = context.ProdutoItems.Find(TId);
            context.ProdutoItems.Remove(p);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Produto> GetEntities()
        {
            return context.ProdutoItems.ToList();
        }

        public Produto GetEntityByID(long TId)
        {
            return context.ProdutoItems.Find(TId);
        }

        public void InsertEntity(Produto t)
        {
            context.ProdutoItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(Produto t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public IEnumerable<string> GetDescricoes() {
            return context.ProdutoItems.Select(o=> o.designacao.designacao).ToList();
        }
    }
}