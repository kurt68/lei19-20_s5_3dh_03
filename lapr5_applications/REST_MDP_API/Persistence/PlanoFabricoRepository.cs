using System;
using System.Collections.Generic;
using REST_MDP_API.Model;
using System.Linq;
namespace REST_MDP_API.Persistence
{
    public class PlanoFabricoRepository : IRepository<PlanoFabrico>
    {
        public mdpContext context;

        public PlanoFabricoRepository(mdpContext context)
        {
            this.context = context;
        }

        public void DeleteEntity(long TId)
        {
            PlanoFabrico p = context.PlanoFabricoItems.Find(TId);
            context.PlanoFabricoItems.Remove(p);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PlanoFabrico> GetEntities()
        {
            return context.PlanoFabricoItems.ToList();
        }

        public PlanoFabrico GetEntityByID(long TId)
        {
            return context.PlanoFabricoItems.Find(TId);
        }

        public void InsertEntity(PlanoFabrico t)
        {
            context.PlanoFabricoItems.Add(t);
        }

        public void save()
        {
            context.SaveChanges();
        }

        public void UpdateEntity(PlanoFabrico t)
        {
            context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }
    }
}