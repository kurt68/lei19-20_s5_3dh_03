using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using REST_MDP_API.Model;
using REST_MDP_API.DTO;
using REST_MDP_API.Service;
using System.Net.Http;
using System;
using Newtonsoft.Json;

namespace REST_MDP_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly mdpContext _context;
        private ProdutoService service;

        public ProdutoController(mdpContext context)
        {
            _context = context;
            service = new ProdutoService(context);

        }

        // GET: api/Produto
        [HttpGet("getDesignacoes")]
        public IEnumerable<string> getDesignacoes()
        {
            return this.service.getDescricoes();
        }

        // GET: api/Produto
        [HttpGet]
        public async Task<IActionResult> GetTodoItems()
        {
            List<Produto> list = await _context.ProdutoItems.Include(p => p.planoFabrico).ThenInclude(pl => pl.operacoes).ToListAsync();
            List<ProdutoDTO> produtosDto = new List<ProdutoDTO>();
            ProdutoDTO dto;
            foreach (Produto p in list)
            {


                LinkedList<OperacaoDTO> opDTos = new LinkedList<OperacaoDTO>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://mdf3hgrupo3.azurewebsites.net/api/");

                    HttpClientHandler clientHandler = new HttpClientHandler();
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                    {
                        return true;
                    };

                    LinkedList<IdOperacao> ids = p.planoFabrico.operacoes;

                    foreach (var idO in ids)
                    {
                        var response = await client.GetAsync("Operacao/" + idO.idOperacao + "/id");

                        if (!response.IsSuccessStatusCode)
                        {
                            return BadRequest("Operações inexistentes");
                        }
                        var responseContent = await response.Content.ReadAsStringAsync();
                        OperacaoDTO content = new OperacaoDTO();
                        content = JsonConvert.DeserializeObject<OperacaoDTO>(responseContent);
                        content.id = idO.idOperacao;
                        opDTos.AddLast(content);

                    }
                    dto = service.objToDTO(p);
                    dto.planoFabrico.operacoes = opDTos;
                    produtosDto.Add(dto);
                }

            }
            return Ok(produtosDto);
        }

        // GET: api/Produto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodoItem([FromRoute]long id)
        {
            Produto todoItem = await _context.ProdutoItems.Include(p => p.planoFabrico).ThenInclude(pl => pl.operacoes).Where(a => a.Id == id).SingleAsync();
            if (todoItem == null)
            {
                return NotFound();
            }
            LinkedList<OperacaoDTO> opDTos = new LinkedList<OperacaoDTO>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://mdf3hgrupo3.azurewebsites.net/api/");

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                LinkedList<IdOperacao> ids = todoItem.planoFabrico.operacoes;

                foreach (var idO in ids)
                {
                    var response = await client.GetAsync("Operacao/id/" + idO.idOperacao);

                    if (!response.IsSuccessStatusCode)
                    {
                        return BadRequest("Operações inexistentes");
                    }

                    var responseContent = await response.Content.ReadAsStringAsync();
                    OperacaoDTO content = new OperacaoDTO();
                    content = JsonConvert.DeserializeObject<OperacaoDTO>(responseContent);
                    content.id = idO.idOperacao;
                    opDTos.AddLast(content);
                }
            }
            ProdutoDTO dto = service.objToDTO(todoItem);
            dto.planoFabrico.operacoes = opDTos;
            return Ok(dto);
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<IActionResult> PostTodoItem(ProdutoDTO item)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://mdf3hgrupo3.azurewebsites.net/api/");

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                LinkedList<OperacaoDTO> ids = item.planoFabrico.operacoes;
                foreach (var id in ids)
                {
                    string resp = "" + id.designacao + "," + id.ferramenta;
                    var response = await client.GetAsync("Operacao/" + resp);

                    if (!response.IsSuccessStatusCode)
                    {
                        return BadRequest("Operações inexistentes");
                    }
                    var responseContent = await response.Content.ReadAsStringAsync();

                    long content = JsonConvert.DeserializeObject<long>(responseContent);
                    id.id = content;

                }
                item.planoFabrico.operacoes = ids;
            }
            Produto o = service.DTOtoObj(item);
            _context.ProdutoItems.Add(o);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTodoItem), new { id = o.Id }, o);
        }

        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem([FromRoute]long id, ProdutoDTO item)
        {
            Produto p = service.findByID(id);
            if (id != p.Id)
            {
                return BadRequest();
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://mdf3hgrupo3.azurewebsites.net/api/");

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                LinkedList<OperacaoDTO> ids = item.planoFabrico.operacoes;

                foreach (var idO in ids)
                {
                    var response = await client.GetAsync("Operacao/" + idO.id);

                    if (!response.IsSuccessStatusCode)
                    {
                        return BadRequest("Operações inexistentes");
                    }


                }
            }
            service.updateObj(p, item);
            _context.Entry(p).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(service.objToDTO(p));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem([FromRoute]long id)
        {
            Produto o = service.findByID(id);
            if (id != o.Id)
            {
                return BadRequest();
            }
            _context.Remove(o);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpGet("GetPlanoProduto/{id}")]
        public async Task<IActionResult> GetPlanoProduto([FromRoute] string id)
        {
            Produto produto = service.findByNome(id);
            if (produto == null) return NotFound("Product not found!");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://mdf3hgrupo3.azurewebsites.net/api/");

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                {
                    return true;
                };

                LinkedList<IdOperacao> ids = produto.planoFabrico.operacoes;

                foreach (var idO in ids)
                {
                    var response = await client.GetAsync("Operacao/" + idO);

                    if (!response.IsSuccessStatusCode)
                    {
                        return BadRequest("Operações inexistentes");
                    }

                    var responseContent = await response.Content.ReadAsStringAsync();

                    List<OperacaoDTO> content = new List<OperacaoDTO>();
                    content = JsonConvert.DeserializeObject<List<OperacaoDTO>>(responseContent);
                }
                return Ok(service.objToDTO(produto));
            }
        }
    }
}