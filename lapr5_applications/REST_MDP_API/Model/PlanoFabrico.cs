using System.Collections.Generic;

namespace REST_MDP_API.Model
{
    public class PlanoFabrico
    {
        public long Id { get; set; }
        public LinkedList<IdOperacao> operacoes { get; set; }

        public PlanoFabrico()
        {
            this.operacoes = new LinkedList<IdOperacao>();
        }

        public PlanoFabrico(LinkedList<IdOperacao> operacoes)
        {
            this.operacoes = operacoes;
        }

    }
}