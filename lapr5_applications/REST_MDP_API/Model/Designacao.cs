using System.Collections.Generic;
namespace REST_MDP_API.Model
{
    public class Designacao : ValueObject
    {
        public string designacao {get; set;}

        public Designacao(){}

        public void setValue(string value)
        {
            this.designacao = value;
        }

        public string valueOf()
        {
            return this.designacao;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new System.NotImplementedException();
        }
    }
}