namespace REST_MDP_API.Model
{
    public class Produto
    {
        public long Id {get; set;}
        public Designacao designacao {get; set;}
        public PlanoFabrico planoFabrico {get; set;}

        public Produto()
        {
            designacao = new Designacao();
            planoFabrico= new PlanoFabrico();
        }

        public Produto(Designacao designacao, PlanoFabrico planoFabrico)
        {
            this.designacao = designacao;
            this.planoFabrico = planoFabrico;
        }

    }
}