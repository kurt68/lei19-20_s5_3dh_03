using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
namespace REST_MDP_API.Model
{
    [Owned]    
    public class IdOperacao : ValueObject
    {
        public long id {get; set;}
        public long idOperacao { get; set; }
        public PlanoFabrico planoFabrico { get; set; }
        public IdOperacao(){}

        public void setValue(long value)
        {
            this.idOperacao = value;
        }

        public long valueOf()
        {
            return this.idOperacao;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return idOperacao;
            yield return planoFabrico;
        }
    }
}