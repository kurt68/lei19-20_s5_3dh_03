const database = require('mongoose');

class BaseRepository {

    constructor(model) {
        this.model = database.model(model);
    }

    async findAll() {
        return await this.model.find();
    }

    async findId(id) {
        return await this.model.findById(id);
    }

    async findOne(obj) {
        return await this.model.findOne(obj);
    }

	async save(obj) {
		return await obj.save();
    }
	
    async remove(id) {
        return await this.model.findByIdAndDelete(id);
    }

    async update(id, obj) {
        return await this.model.updateOne({_id: id}, obj);
    }

}

module.exports = BaseRepository;