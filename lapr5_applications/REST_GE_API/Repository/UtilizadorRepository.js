const BaseRepository = require("./BaseRepository");
class UtilizadorRepository extends BaseRepository {

    constructor(model) {
        super(model);
    }
    async findOne(utilizadorDTO) {
        let username = utilizadorDTO.username;
        let candidatePassword = utilizadorDTO.password;

        let user = await this.model.findOne({ username: username });

        if (!user) {
            return null;
        }

        // test a matching password
        let match = await user.comparePassword(candidatePassword);
        if (match) {
            return user;
        }
        return null;
    }

    async findByUsername(username) {

        let user = await this.model.findOne({ username: username });

        if (!user) {
            return null;
        }
        return user;
    }

    async findAllByName() {
        return await this.model.find().select({'name': 1, 'username': 1, '_id': 0, 'role':1}).exec();
    }

    async findByUsernameData(username) {

        let user = await this.model.findOne({ username: username }).select({'name': 1,'username': 1, 'email': 1, '_id': 0});

        if (!user) {
            return null;
        }
        return user;
    }
}

module.exports = UtilizadorRepository;



