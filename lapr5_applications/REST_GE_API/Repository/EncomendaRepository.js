const BaseRepository = require("./BaseRepository");
class EncomendaRepository extends BaseRepository {

    constructor(model) {
        super(model);
    }

    async findOne(encomendaDTO) {
        let codigo = encomendaDTO.codigo;
        
        let encomenda = await this.model.findOne({codigo: codigo});

        if(!encomenda) {
            return null;
        }
        return encomenda;
    }

    async findByUsername(username) {
        let encomendas = await this.model.find({username : username});
        return encomendas;
    }

    async findByCodigo(codigo) {
        let encomendas = await this.model.find({codigo : codigo});
        return encomendas;
    }
}
module.exports = EncomendaRepository;

