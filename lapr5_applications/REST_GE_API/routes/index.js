var express = require('express');
var router = express.Router();
const users = require('./users');
const encomendas = require('./encomendas');
const planoProducao = require('./planoProducao');


router.use('/users', users);
router.use('/encomendas', encomendas);
router.use('/planoProducao', planoProducao);

module.exports = router;
