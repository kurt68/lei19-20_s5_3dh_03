var express = require('express');
var router = express.Router();
const controller = require('../Controller/UtilizadorController');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const middleware = require('../middleware');

/* POST users listing. */
//router.post('/login', controller.login);

router.post('/signup', async (req, res) => {
  try {
    let user = req.body;

    controller.signup(user);
    res.status(201).json({ message: 'Utilizador Criado' });
  } catch (error) {

    res.status(500);

  }
});

router.post('/login', async (req, res) => {
  try {
    let user = req.body
    let x = await controller.login(user);
    if (x == null) { res.status(500).json({ message: 'Erro no Log In' }); }

    res.status(200).json({ x });

  } catch (error) {
    res.status(500);
  }

});

router.put('/alterar', middleware.checkToken, async (req, res) => {
  try {
    let user = req.body
    controller.alterar(user, req.decoded.username);
    res.status(201).json({ message: 'Utilizador Alterado' });
  } catch (error) {
    res.status(500).json({ message: 'Erro a Alterar' });
  }

});

router.get('/consultar', middleware.checkToken, async (req, res) => {
  try {
    if (req.decoded.role === "Admin") {
      var user = await controller.consultar();
      res.status(200).json({ message: 'Acesso a dados permitido', user });
    } else {
      res.status(500).json({ message: 'Não tem permissão para consultar' });
    }
  } catch (error) {
    res.status(500).json({ message: 'Erro a Consultar' });
  }
});

router.get('/consultar/cliente', middleware.checkToken, async (req, res) =>{
  try {
      var user = await controller.consultarDados(req.decoded.username);
      res.status(200).json({ message: 'Acesso a dados permitido', user });
  } catch (error) {
    res.status(500).json({ message: 'Erro a Consultar' });
  }
})



module.exports = router;
