var express = require('express');
var router = express.Router();
const controller = require('../Controller/EncomendaController');
const controllerEnco = require('../Controller/PlanoProducaoController');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const middleware = require('../middleware');

router.post("/encomenda", middleware.checkToken, async (req, res) => {
    try {
        let itens = req.body;
        if (req.decoded.role == "Admin") {
            return res.status(403).json({ message: 'Nao tem permissao para criar encomenda' });
        }
        await controller.criar(itens);
        var x = [];
        x.encomendas = await controller.consultarEncomendasPorProduzir();
        await controllerEnco.alterar(x);
        res.status(201).json({ message: 'Encomenda Criada' });

    } catch (error) {
        res.status(500);

    }
});

router.put("/encomenda/:id", middleware.checkToken, async (req, res) => {
    try {
        let id = req.params.id;
        let itens = req.body;
        if (req.decoded.role == "Cliente") {
            return res.status(403).json({ message: 'Nao tem permissao para alterar encomenda' });
        }

        await controller.alterar(itens, id);
        return res.status(200).json({ message: 'Encomenda alterada' });
    } catch (error) {
        res.status(500).json({ message: error });
    }
})

router.get("/encomenda", middleware.checkToken, async (req, res) => {
    try {
        if (req.decoded.role == "Cliente") {
            return res.status(200).json(await controller.consultarClienteByUsername(req.decoded.username));
        }
        if (req.decoded.role == "Admin") {
            return res.status(200).json(await controller.adminConsultar());
        }
    } catch (error) {
        res.status(500).json({ message: error });
    }
})

router.get("/encomenda/produtoInfo/:designacao", middleware.checkToken, async (req, res) => {
    try {
        let designacao = req.params.designacao;
        return res.status(200).json(await controller.produtoInfo(designacao));
    } catch (error) {
        res.status(500).json({ message: error });
    }
})

router.put("/encomenda/cancelar/:id", middleware.checkToken, async (req, res) => {
    try {
        let id = req.params.id;
        if (req.decoded.role == "Cliente") {
            return res.status(403).json({ message: 'Nao tem permissao para cancelar encomenda' });
        }

        await controller.cancelar(id);
        return res.status(200).json({ message: 'Encomenda cancelada' });
    } catch (error) {
        res.status(500).json({ message: error });
    }
})

router.get("/encomendasProduzir", middleware.checkToken, async (req, res) => {
    try {
        return res.status(200).json(await controller.consultarEncomendasPorProduzir());
    } catch (error) {
        res.status(500).json({ message: error });
    }
})

module.exports = router;
