var express = require('express');
var router = express.Router();
const controller = require('../Controller/PlanoProducaoController');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const middleware = require('../middleware');

router.put("/plano", middleware.checkToken, async (req, res) => {
    try {
        let itens = req.body;
        if (req.decoded.role == "Cliente") {
            return res.status(403).json({ message: 'Nao tem permissao' });
        }
        await controller.alterar(itens);
        return res.status(200).json({ message: 'Plano produção alterado' });
    } catch (error) {
        res.status(500).json({ message: error });
    }
});

router.get("/plano", middleware.checkToken, async (req, res) => {
    try {
        if (req.decoded.role == "Cliente") {
            return res.status(403).json({ message: 'Nao tem permissao' });
        }
        if (req.decoded.role == "Admin") {
            return res.status(200).json(await controller.consultar());
        }
    } catch (error) {
        res.status(500).json({ message: error });
    }
});

module.exports = router;