const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const PlanoProducao = require('../Model/PlanoProducao');
const BaseRepository = require('../Repository/BaseRepository')
const repository = new BaseRepository(PlanoProducao.modelName);
const promiseResult = require('../utils/to');



const PlanoProducaoService = (function () {

    return {

        alterar: async function (novo, Original) {
            
            let planoProducao = new PlanoProducao();
            planoProducao.listaEncomendas = novo;
            var ori = Original[0];

            [err, result] = await promiseResult.to(repository.update(ori._id, planoProducao));
            if (err) {
                throw (err);
            }
            return result;
        },

        Consultar: async function () {
            [err, result] = await promiseResult.to(repository.findAll());
            if (err) {
                throw (err);
            }
            return result;
        }
    }
})();
module.exports = PlanoProducaoService;
