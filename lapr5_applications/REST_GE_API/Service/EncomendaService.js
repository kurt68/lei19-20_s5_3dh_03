const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Encomenda = require("../Model/Encomenda");
const EncomendaRepository = require("../Repository/EncomendaRepository");
const repository = new EncomendaRepository(Encomenda.modelName);
const EncomendaDTO = require('../DTO/EncomendaDTO');
const promiseResult = require('../utils/to');
const Estado = require('../Model/Estado');


const EncomendaService = (function () {

    return {

        criar: async function (EncomendaDTO) {
            let codigo = EncomendaDTO.codigo;
            let username = EncomendaDTO.username;
            let produto = EncomendaDTO.produto;
            let quantidade = EncomendaDTO.quantidade;
            let morada = EncomendaDTO.morada;

            let encomenda = new Encomenda();
            encomenda._id = new mongoose.Types.ObjectId();
            encomenda.codigo = codigo;
            encomenda.username = username;
            encomenda.produto = produto.designacao;
            encomenda.quantidade = quantidade;
            encomenda.morada = morada;

            await repository.save(encomenda);
            return EncomendaDTO;
        },

        findById: async function (id) {
            return repository.findId(id);
        },

        alterar: async function (EncomendaDTO, Original) {
            let estado = EncomendaDTO.estado;
            let username = EncomendaDTO.username;
            let produto = EncomendaDTO.produto.designacao;
            let quantidade = EncomendaDTO.quantidade;
            let morada = EncomendaDTO.morada;

            let encomenda = new Encomenda();
            encomenda.estado = estado;
            encomenda.username = username;
            encomenda.produto = produto;
            encomenda.quantidade = quantidade;
            encomenda.morada = morada;

            [err, result] = await promiseResult.to(repository.update(Original._id, encomenda));
            if (err) {
                throw (err);
            }
            return result;
        },

        adminConsultar: async function () {
            [err, result] = await promiseResult.to(repository.findAll());
            if (err) {
                throw (err);
            }
            return result;
        },

        consultarClienteByUsername: async function (username) {
            [err, result] = await promiseResult.to(repository.findByUsername(username));
            if (err) {
                throw (err);
            }
            return result;
        },
        cancelar: async function (Original) {
            Original.estado = Estado.Cancelada;
            
            [err, result] = await promiseResult.to(repository.update(Original._id, Original));
            if (err) {
                throw (err);
            }
            return result;
        },
        produtoInfo: async function(designacao){
            [err, result] = await promiseResult.to(repository.findAll());
            if (err) {
                throw (err);
            }
            let cont = 0;
            let qt = 0;
            for (let index = 0; index < result.length; index++) {
                if(result[index].produto.toUpperCase() == designacao.toUpperCase() && result[index].estado.toUpperCase() != "CANCELADA"){
                    cont++;
                    qt += result[index].quantidade;
                }
            
            }
            var produtoInfo = [];
            produtoInfo.push(qt);
            produtoInfo.push(cont);
            return produtoInfo;    
        },
        consultarEncomendasPorProduzir: async function(){
            [err, result] = await promiseResult.to(repository.findAll());
            var encomendas = [];
            if (err) {
                throw (err);
            }
            result.forEach(element => {
                if (element.estado.toUpperCase() != 'CANCELADA' && element.estado.toUpperCase() != 'ENTREGUE') {
                    encomendas.push(element);
                }
            });
            return encomendas;
        },
        alterarDataConclusao: async function(novo, Original){
            [err, result] = await promiseResult.to(repository.update(Original._id, novo));
            if (err) {
                throw (err);
            }
            return result;
        }
    }
})();
module.exports = EncomendaService;
