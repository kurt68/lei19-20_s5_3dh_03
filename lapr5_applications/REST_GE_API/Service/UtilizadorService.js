const UtilizadorRepository = require('../Repository/UtilizadorRepository');
const promiseResult = require('../utils/to');
const Utilizador = require('../Model/Utilizador');
const Role = require('../Model/Role');
const RegisteredUserDTO = require('../DTO/UtilizadorDTO');
const repository = new UtilizadorRepository(Utilizador.modelName);
const SECRET_LENGTH = 10;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UtilizadorService = (function () {

    return {

        signUp: async function (UtilizadorSignUpDTO) {
            let username = UtilizadorSignUpDTO.username;
            let name = UtilizadorSignUpDTO.name;
            let email = UtilizadorSignUpDTO.email;
            let password = UtilizadorSignUpDTO.password;

            let user = new Utilizador();
            user._id = new mongoose.Types.ObjectId(),
                user.username = username,
                user.password = password,
                user.email = email,
                user.name = name,
                user.role = Role.Cliente

            await repository.save(user);
            return new RegisteredUserDTO(username);
        },

        login: async function (userLoginDTO) {
            return await repository.findOne(userLoginDTO);
        },

        alterar: async function (utilizadorDTO, username) {
            var result;
            var user = await repository.findByUsername(username);
            if (user == null) {
                return null;
            } else {
                var newUser = new Utilizador({
                    _id: user._id,
                    name: user.name,
                    username: utilizadorDTO.username,
                    password: user.password,
                    email: utilizadorDTO.email,
                    role: user.role
                });
                [err, result] = await promiseResult.to(repository.update(user._id, newUser));
            }
            if(err){
                throw(err);
            }
            return result;
        },

        consultar: async function () {
            var user;
            [err, user] = await promiseResult.to(repository.findAllByName());
            var clientes = [];
            user.forEach(element => { 
                if(element.role.toUpperCase() == "CLIENTE") {
                    clientes.push(element);
                }     
            });
            if(err){
                throw(err);
            }
            return clientes;
        },

        consultarDados: async function(username){
            var dados;
            [err, dados] = await promiseResult.to(repository.findByUsernameData(username));
            if(err){
                throw(err);
            }
            return dados;
        },
        alterarPrioridade: async function(username, pri) {
            var result;
            var user = await repository.findByUsername(username);
            if (user == null) {
                return null;
            } else {
                var newUser = new Utilizador({
                    _id: user._id,
                    name: user.name,
                    username: user.username,
                    password: user.password,
                    email: user.email,
                    role: user.role,
                    prioridade: pri
                });
                [err, result] = await promiseResult.to(repository.update(user._id, newUser));
            }
            if(err){
                throw(err);
            }
            return result;

        }
    }
}) ();

module.exports = UtilizadorService;