const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Estado = require('./Estado');

const schema = new Schema({

    _id: mongoose.Schema.Types.ObjectId,
    codigo: {
        type: String,
        required: true,
        unique: true,
    },
    estado: {
        type: Estado,
        enum: Object.values(Estado),
        default: Estado.Submetida,
    },
    produto: {
        type: String,
        required: true,
    },
    quantidade: {
        type: Number,
        required: true,
        minlength : 1,
    },
    username: {
        type: String,
        required: true
    },
    morada: {
        type: String,
        required: true
    },
    data_encomenda: {
        type: Date,
        default: Date.now
    },
    data_conclusao: {
        type: Date
    } 
}, {
    versionKey: false
});

module.exports = mongoose.model('Encomenda', schema);