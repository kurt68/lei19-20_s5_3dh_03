const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Encomenda = require('./Encomenda').schema;

const schema = new Schema({

    _id: mongoose.Schema.Types.ObjectId,
    
    listaEncomendas: [Encomenda]
}, {
    versionKey: false
});

module.exports = mongoose.model('PlanoProducao', schema);