class EncomendaDTO {
    
    constructor(codigo, username, estado, produto, quantidade, morada, data_encomenda) {
        this.codigo = codigo;
        this.username = username;
        this.estado = estado;
        this.produto = produto;
        this.quantidade = quantidade;
        this.morada = morada;
        this.data_encomenda = data_encomenda;
    }
}
module.exports = EncomendaDTO;