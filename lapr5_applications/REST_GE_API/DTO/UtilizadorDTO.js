class UtilizadorDTO {

    constructor(name, username, email, password, prioridade) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.prioridade = prioridade;
    }
}

module.exports = UtilizadorDTO;