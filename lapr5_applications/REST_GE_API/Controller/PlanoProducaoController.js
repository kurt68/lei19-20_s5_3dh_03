const encomendaService = require('../Service/PlanoProducaoService');
const jwt = require('jsonwebtoken');
const speakeasy = require('speakeasy');
const EncomendaController = require('./EncomendaController')

let PlanoProducaoController = (function () {

    return {
        alterar: async function (novo) {
            try {
                const original = await encomendaService.Consultar();
                var novaOrdem = novo.encomendas.slice(0);
                var alterarData = novo;
                await EncomendaController.alterarDataConclusao(alterarData);
                return await encomendaService.alterar(novaOrdem, original);
            } catch (error) {
                throw error;
            }
        },
        consultar: async function () {
            try {
                return await encomendaService.Consultar();
            } catch (error) {
                throw error;
            }
        }
    }
})();
module.exports = PlanoProducaoController;