const encomendaService = require('../Service/EncomendaService');
const jwt = require('jsonwebtoken');
const speakeasy = require('speakeasy');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

let EncomendaController = (function () {

    return {

        criar: async function (encomendaDTO) {
            try {
                return await encomendaService.criar(encomendaDTO);

            } catch (e) {
                throw e;
            }
        },
        alterar: async function (encomendaDTO, id) {
            try {
                const original = await encomendaService.findById(id);

                return await encomendaService.alterar(encomendaDTO, original);
            } catch (error) {
                throw error;
            }
        },
        adminConsultar: async function () {
            try {
                return await encomendaService.adminConsultar();
            } catch (error) {
                throw error;
            }
        },
        consultarClienteByUsername: async function (username) {
            try {
                return await encomendaService.consultarClienteByUsername(username);
            } catch (error) {

            }
        },
        cancelar: async function (id) {
            try {
                const original = await encomendaService.findById(id);

                return await encomendaService.cancelar(original);
            } catch (error) {
                throw error;
            }
        },
        produtoInfo: async function (designacao) {
            try {
                return await encomendaService.produtoInfo(designacao);
            } catch (error) {
                throw error;
            }
        },
        consultarEncomendasPorProduzir: async function () {
            try {
                return await encomendaService.consultarEncomendasPorProduzir();
            } catch (error) {
                throw error;
            }
        },
        alterarDataConclusao: async function (ordem) {
            const result = await encomendaService.adminConsultar();
            var responseText = httpGet('https://mdp3hgrupo3.azurewebsites.net/api/Produto');
            var produtos = JSON.parse(responseText);

            let index = 0;
            var sumTime = 0;
            while (ordem.encomendas.length > 0) {
                if (result[index].codigo == ordem.encomendas[0].codigo) {
                    var sumT = 0;
                    var encomendaAlterar = ordem.encomendas.shift();
                    var alterada = encomendaAlterar;
                    alterada.data_conclusao = new Date();
                    produtos.forEach(element => {
                        if (element.designacao.toUpperCase() == alterada.produto.toUpperCase()) {
                            element.planoFabrico.operacoes.forEach(element => {
                                sumT = sumT + element.duracao;    
                            });    
                        }
                    });
                    sumTime = sumTime + sumT * alterada.quantidade;
                    alterada.data_conclusao.setSeconds(alterada.data_conclusao.getSeconds() + sumTime);
                    await encomendaService.alterarDataConclusao(alterada, encomendaAlterar);
                    index = -1;
                }
                index++;
            }

        }
    }
})();

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}
module.exports = EncomendaController;