const userService = require('../Service/UtilizadorService');
const jwt = require('jsonwebtoken');
const UtilizadorSignUpDTO = require('../DTO/UtilizadorSignUpDTO');
const config = require('../config');

let UtilizadorController = (function () {

    return {

        login: async function (userLoginDTO) {
            let user = await userService.login(userLoginDTO);

            if (user != null) {
                let token = jwt.sign({ username: user.username, role: user.role },
                    config.secret,
                    {
                        expiresIn: '30m' // expires in 30 minutes
                    }
                );
                // return the JWT token for the future API calls
                return {
                    access_token: token,
                    expiration: new Date(new Date().getTime() + 30 * 60000),
                    username: user.username,
                    role: user.role
                };
            }
            return null;
        },

        signup: async function (userSignUpDTO) {
            try {
                return await userService.signUp(userSignUpDTO);

            } catch (e) {
                throw e;
            }
        },

        alterar: async function (utilizadorDTO, username) {
            try {
                return await userService.alterar(utilizadorDTO, username);
            } catch (e) {
                throw e;
            }
        },

        consultar: async function () {
            try {
                var consulta = await userService.consultar();
                return consulta;
            } catch (e) {
                throw e;
            }
        },

        consultarDados: async function(username){
            try {
                var consulta = await userService.consultarDados(username);
                return consulta;
            } catch (e) {
                throw e;
            }
        },

        alterarPrioridade: async function(username, pri) {
            try{
                return await userService.alterarPrioridade(username, pri); 
            } catch(e) {
                throw e;
            }
        }

    }

})();

module.exports = UtilizadorController;
