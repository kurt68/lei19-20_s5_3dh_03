//function that returns the result of a promise
exports.to =  function to(promise) {
    return promise.then(data => {
       return [null, data];
    })
    .catch(err => [err]);
 }
