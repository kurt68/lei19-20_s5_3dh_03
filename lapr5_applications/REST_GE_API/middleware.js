let jwt = require('jsonwebtoken');
const config = require('./config.js');

let checkToken = (req, res, next) => {
    let token = req.headers['authorization']; // Express headers are auto converted to lowercase
    if(token && (token = token.slice("Bearer ".length, token.length))) {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) {
                    return res.status(401).json({
                        mensagem: 'Token não é válido'
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
    } else {
        return res.status(401).json({
            mensagem: 'Token não foi fornecido'
        });
    }
    };

module.exports = {
    checkToken: checkToken
};