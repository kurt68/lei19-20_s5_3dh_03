import { AppPage } from './cancelarEncomenda.po';

describe('Cancelar Encomenda', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Encomendas existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });
});