import { AppPage } from './postOperacao.po';

describe('Create Operacao', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be MY OWN CUTLERY', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });
});
