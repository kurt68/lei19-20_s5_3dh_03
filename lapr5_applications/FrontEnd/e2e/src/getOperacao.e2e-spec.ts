import { AppPage } from './getOperacao.po';

describe('Get Operacao', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Operacoes existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('Operacoes existentes:');
  });

});