import { AppPage } from './putEncomenda.po';

describe('Editar Encomenda', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Editar encomenda', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });
});