import { AppPage } from './ordenarEncomenda.po';

describe('Ordenar Encomendas', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Ordenar Encomendas:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });
});