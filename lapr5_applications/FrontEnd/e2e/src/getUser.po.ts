import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/user/get');
  }

  getTittleText() {
    return element(by.css('h1')).getText();
  }
}