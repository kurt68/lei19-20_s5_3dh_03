import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/maquina/get');
  }

  getTittleText() {
    return element(by.css('h4')).getText();
  }
}