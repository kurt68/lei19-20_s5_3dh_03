import { AppPage } from './putTipoMaquina.po';

describe('Put TipoMaquina', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be MY OWN CUTLERY', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });

});