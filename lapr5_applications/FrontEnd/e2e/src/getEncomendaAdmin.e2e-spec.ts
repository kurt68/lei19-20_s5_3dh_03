import { AppPage } from './getEncomendaAdmin.po';

describe('Get Encomenda Admin', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Encomendas existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });
});