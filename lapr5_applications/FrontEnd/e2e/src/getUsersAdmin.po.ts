import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/user/getAll');
  }

  getTittleText() {
    return element(by.css('h1')).getText();
  }
}