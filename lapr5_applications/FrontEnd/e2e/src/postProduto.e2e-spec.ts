import { AppPage } from './postProduto.po';

describe('Post Produto', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be MY OWN CUTLERY', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('MY OWN CUTLERY');
  });

});