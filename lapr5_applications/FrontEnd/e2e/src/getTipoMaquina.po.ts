import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/tipoMaquina/get');
  }

  getTittleText() {
    return element(by.css('h4')).getText();
  }
}