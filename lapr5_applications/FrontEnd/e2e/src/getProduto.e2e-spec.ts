import { AppPage } from './getProduto.po';

describe('Get Produto', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Produtos existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('Produtos existentes:');
  });

});