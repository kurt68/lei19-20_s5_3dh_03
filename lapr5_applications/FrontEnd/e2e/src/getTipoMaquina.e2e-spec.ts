import { AppPage } from './getTipoMaquina.po';

describe('Get TipoMaquina', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Tipos de Maquinas existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('Tipos de Maquinas existentes:');
  });

});