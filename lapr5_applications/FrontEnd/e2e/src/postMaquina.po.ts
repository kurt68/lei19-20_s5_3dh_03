import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/maquina/post');
  }

  getTittleText() {
    return element(by.css('h1')).getText();
  }
}