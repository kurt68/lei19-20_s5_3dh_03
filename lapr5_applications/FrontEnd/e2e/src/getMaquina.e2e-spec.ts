import { AppPage } from './getMaquina.po';

describe('Get Maquina', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Tittle button should be Maquinas existentes:', () => {
    page.navigateTo();
    expect(page.getTittleText()).toEqual('Maquinas existentes:');
  });
});