import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/operacao/get');
  }

  getTittleText() {
    return element(by.css('h4')).getText();
  }
}