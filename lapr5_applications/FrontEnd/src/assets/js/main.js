import * as THREE from "three";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { GUI } from "three/examples/jsm/libs/dat.gui.module.js";
import { DragControls } from "three/examples/jsm/controls/DragControls.js";

function httpGet(theUrl) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, false); // false for synchronous request
  xmlHttp.send(null);
  return xmlHttp.responseText;
}

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
var produtos = [];

init();
export function init() {
  var responseText = httpGet('https://mdf3hgrupo3.azurewebsites.net/api/LinhaProducao');
  var linhasProducao = JSON.parse(responseText);
  console.log(linhasProducao);


  scene.background = null;



  var pointLight = new THREE.PointLight(0xffffff, 0.8, 18);
  pointLight.position.set(-3, 6, -3);
  pointLight.castShadow = true;
	// Will not light anything closer than 0.1 units or further than 25 units
	pointLight.shadow.camera.near = 0.1;
	pointLight.shadow.camera.far = 25;
  var ambientLight = new THREE.AmbientLight(0xffffff, 3.0);
  scene.add(ambientLight);
  scene.add(pointLight);

  var renderer = new THREE.WebGLRenderer({ alpha: true });
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  var controls = new OrbitControls(camera, renderer.domElement);
  controls.addEventListener('change', function () { renderer.render(scene, camera); });

  //definição dos controls
  controls = new OrbitControls(camera, renderer.domElement);

  controls.target = new THREE.Vector3(0, 0, 0);
  controls.maxPolarAngle = Math.PI / 2 - 0.15;
  controls.minDistance = -10;
  controls.addEventListener('change', light_update);

  //definição das luzes
  var light = new THREE.AmbientLight(0x404040); // soft white light
  light.castShadow = false;
  scene.add(light);
  var directionalLight = new THREE.DirectionalLight(0xffffff, 5.0);
  scene.add(directionalLight);
  var objects = [];
  var dragControls = new DragControls(objects, camera, renderer.domElement);
  dragControls.addEventListener('dragstart', function () {
    controls.enabled = false;
  });
  dragControls.addEventListener('dragend', function () {
    controls.enabled = true;
  });

  function light_update() {
    scene.remove(directionalLight);

    directionalLight.position.copy(camera.position);
    directionalLight.castShadow = true;
    scene.add(directionalLight);
  }

  //configração da textura do chão
  var floorMat = new THREE.MeshStandardMaterial({
    roughness: 0.8,
    color: 0xffffff,
    metalness: 0.2,
    bumpScale: 0.0005
  });

  var textureLoader = new THREE.TextureLoader();

  //adicionar a textura do chão ao map
  textureLoader.load("assets/textures/parket.jpg", function (map) {
    map.wrapS = THREE.RepeatWrapping;
    map.wrapT = THREE.RepeatWrapping;
    map.anisotropy = 4;
    map.repeat.set(10, 24);
    floorMat.map = map;
    floorMat.needsUpdate = true;
  });

  //criar o plano para o chão
  var geo = new THREE.PlaneBufferGeometry(50, 35);
  var plane = new THREE.Mesh(geo, floorMat);
  plane.userData.tooltipText = "chão de fábrica";

  plane.receiveShadow = true;

  scene.add(plane);
  plane.rotateX(-Math.PI / 2);

  var i;
  var yLinhaProdInit = 20.5;
  var index;
  var yMaquina = 24;
  var maquinas = [];
  for (i = 0; i < linhasProducao.length; i++) {
    drawLinhaProducao(0.5, yLinhaProdInit);

    var xMaquina = 9.5;
    console.log(linhasProducao[0].designacao);
    for (index = 0; index < linhasProducao[i].sequenciaMaquinas.length; index++) {
      console.log(index);
      maquinas.push(drawMaquina(yMaquina, xMaquina));
      xMaquina = xMaquina - 5;
    }
    yMaquina = yMaquina - 10;
    yLinhaProdInit = yLinhaProdInit - 10;


    var geometry = new THREE.SphereGeometry(0.5, 32, 32);
    var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
    var sphere = new THREE.Mesh(geometry, material);
    sphere.position.x = yMaquina + 6.5;
    sphere.position.z = 14.5;
    sphere.position.y = 2.75;
    scene.add(sphere);
    produtos.push(sphere);
  }

  camera.position.z = 10;

  scene.add(camera);
}


function onDocumentMouseDown(event) {
  event.preventDefault();
  var mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
  var raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  var intersects = raycaster.intersectObject(scene);

  var matched_marker = null;
  if (intersects.length > 0) {
    //$('html,body').css('cursor','pointer');//mouse cursor change
    for (var i = 0; intersects.length > 0 && i < intersects.length; i++) {
      window.open(intersects[0].object.userData.URL);
    }
  }
  else {
    //$('html,body').css('cursor','cursor');
  }
}

function onDocumentMouseMove(event) {
  var divElement = $("#tooltip");
  var mouse = new THREE.Vector2();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
  var raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  var intersects = raycaster.intersectObject(plane);

  if (intersects.length > 0) {


    // var position = new THREE.Vector3();
    // var quaternion = new THREE.Quaternion();
    // var scale = new THREE.Vector3();
    // hoveredObj.matrix.decompose(position, quaternion, scale);
    divElement.text("aksnd");
    $('html,body').css('cursor', 'move');
  } else {
    $('html,body').css('cursor', 'default');
  }

}
// this will be 2D coordinates of the current mouse position, [0,0] is middle of the screen.
var mouse = new THREE.Vector2();

var latestMouseProjection; // this is the latest projection of the mouse on object (i.e. intersection with ray)
var hoveredObj; // this objects is hovered at the moment

// tooltip will not appear immediately. If object was hovered shortly,
// - the timer will be canceled and tooltip will not appear at all.
var tooltipDisplayTimeout;

// This will move tooltip to the current mouse position and show it by timer.
function showTooltip() {
  var divElement = $("#tooltip");

  if (divElement && latestMouseProjection) {
    divElement.css({
      display: "block",
      opacity: 0.0
    });

    var canvasHalfWidth = renderer.domElement.offsetWidth / 2;
    var canvasHalfHeight = renderer.domElement.offsetHeight / 2;

    var tooltipPosition = latestMouseProjection.clone().project(camera);
    tooltipPosition.x = (tooltipPosition.x * canvasHalfWidth) + canvasHalfWidth + renderer.domElement.offsetLeft;
    tooltipPosition.y = -(tooltipPosition.y * canvasHalfHeight) + canvasHalfHeight + renderer.domElement.offsetTop;

    //var tootipWidth = divElement[0].offsetWidth;
    //var tootipHeight = divElement[0].offsetHeight;

    divElement.css({
      left: `${tooltipPosition.x}px`,
      top: `${tooltipPosition.y}px`
    });

    // var position = new THREE.Vector3();
    // var quaternion = new THREE.Quaternion();
    // var scale = new THREE.Vector3();
    // hoveredObj.matrix.decompose(position, quaternion, scale);
    divElement.text(hoveredObj.userData.tooltipText);

    setTimeout(function () {
      divElement.css({
        opacity: 1.0
      });
    }, 25);
  }
}

// This will immediately hide tooltip.
function hideTooltip() {
  var divElement = $("#tooltip");
  if (divElement) {
    divElement.css({
      display: "none"
    });
  }
}

// Following two functions will convert mouse coordinates
// from screen to three.js system (where [0,0] is in the middle of the screen)
function updateMouseCoords(event, coordsObj) {
  coordsObj.x = ((event.clientX - renderer.domElement.offsetLeft + 0.5) / window.innerWidth) * 2 - 1;
  coordsObj.y = -((event.clientY - renderer.domElement.offsetTop + 0.5) / window.innerHeight) * 2 + 1;
}

function handleManipulationUpdate() {
  var raycaster = new THREE.Raycaster();
  raycaster.setFromCamera(mouse, camera);
  {
    var intersects = raycaster.intersectObject(plane);
    if (intersects.length > 0) {
      latestMouseProjection = intersects[0].point;
      hoveredObj = intersects[0].object;
    }
  }

  if (tooltipDisplayTimeout || !latestMouseProjection) {
    clearTimeout(tooltipDisplayTimeout);
    tooltipDisplayTimeout = undefined;
    hideTooltip();
  }

  if (!tooltipDisplayTimeout && latestMouseProjection) {
    tooltipDisplayTimeout = setTimeout(function () {
      tooltipDisplayTimeout = undefined;
      showTooltip();
    }, 330);
  }
}

function onMouseMove(event) {
  updateMouseCoords(event, mouse);

  latestMouseProjection = undefined;
  hoveredObj = undefined;
  handleManipulationUpdate();
}

window.addEventListener('mousemove', onMouseMove, false);
function drawLinhaProducao(y, x) {
  var mtlLoader = new MTLLoader();
  mtlLoader.load("../textures/scene.mtl", function (materials) {
    materials.preload(); var objLoader = new OBJLoader();
    objLoader.crossOrigins = '';
    objLoader.load("textures/scene.obj", function (object) {
      scene.add(object);
      object.scale.set(0.004, 0.016, 0.003);
      object.position.y = 0;
      object.position.x = x;
      object.position.z = 0;
      object.rotateX(-Math.PI / 2);
    });
  });

}
function drawMaquina(y, x) {
  var mtlLoader = new MTLLoader();
  mtlLoader.load("textures/scene.mtl", function (materials) {
    materials.preload(); var objLoader = new OBJLoader();
    console.log("x"); objLoader.crossOrigins = '';
    objLoader.load("textures/Robot_Hand_Modeling_Sketchfab.obj", function (object) {
      console.log("x");
      scene.add(object);
      object.scale.set(0.5, 0.5, 0.5);
      object.position.y = 1.10;
      object.position.x = y;
      object.position.z = x;
    });
  });
}
function animate() {
  var posMaq = 9.5;
  var renderer = new THREE.WebGLRenderer({ alpha: true });
  renderer.render(scene, camera);
  for (let i = 0; i < produtos.length; i++) {
    if (produtos[i].position.z >= -15) {
      produtos[i].position.z -= 0.01; // You decide on the increment, higher value will mean the objects moves faster
    }
    if (Math.round(produtos[i].position.z * 100) / 100 == Math.round(posMaq * 100) / 100) {
      produtos[i].material.color.setHex(0xFF0000);
    }

  }
  requestAnimationFrame(animate);
}
animate();