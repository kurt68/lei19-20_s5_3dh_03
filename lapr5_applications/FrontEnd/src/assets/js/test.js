import * as THREE from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js"
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import { TransformControls } from "three/examples/jsm/controls/TransformControls.js"
import { GUI } from "three/examples/jsm/libs/dat.gui.module.js";
import $ from "jquery";
import DragControls from 'drag-controls';

DragControls.install({ THREE: THREE });

var camera, scene, renderer;
var controls;
var plane;
//var control;

document.addEventListener('mousemove', onMouseMove, false);

let linhasProducao = [];
let maquinas = [];
let tiposMaquina = [];
let encomendasOrdenadas = [];
let produtos = [];

//Linhas selecionadas na gui para serem criados visualizadas no build
let mostrarLinhas = [];

//Listas de objetos LinhasProducao para animar
let objsLinhas = [];
let objsLinhasPos = [];
let objsProdutos = [];

let objects = [];

export function init(encomendasPara) {
    maquinas = JSON.parse(httpGet('https://mdf3hgrupo3.azurewebsites.net/api/Maquina'));
    console.log(maquinas);
    tiposMaquina = JSON.parse(httpGet('https://mdf3hgrupo3.azurewebsites.net/api/TipoMaquina'));
    produtos = JSON.parse(httpGet('https://mdp3hgrupo3.azurewebsites.net/api/Produto'));

    console.log(encomendasPara.length)
    encomendasOrdenadas = encomendasPara
    console.log(encomendasOrdenadas);

    var responseText = httpGet('https://mdf3hgrupo3.azurewebsites.net/api/LinhaProducao');
    linhasProducao = JSON.parse(responseText);
    console.log(linhasProducao);

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 15;

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);;
    renderer = new THREE.WebGLRenderer({ antialias: true });

    controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change', function () { renderer.render(scene, camera); });

    controls.maxPolarAngle = Math.PI / 2 - 0.15;


    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    draw();
    console.log(maquinas1);

    animate();
    buildGUI();
}

function draw() {


    var floorMat = new THREE.MeshStandardMaterial();
    var textureLoader = new THREE.TextureLoader();

    //adicionar a textura do chão ao map
    textureLoader.load("assets/textures/parket.jpg", function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(10, 24);
        floorMat.map = map;
        floorMat.needsUpdate = true;
    });

    var geo = new THREE.PlaneBufferGeometry(50, 35);
    plane = new THREE.Mesh(geo, floorMat);
    plane.receiveShadow = true;
    scene.add(plane);



    plane.rotateX(-Math.PI / 2);
    var pointLight = new THREE.PointLight(0xffffff, 0.8, 18);
    pointLight.position.set(-3, 6, -3);
    pointLight.castShadow = true;
    // Will not light anything closer than 0.1 units or further than 25 units
    pointLight.shadow.camera.near = 0.1;
    pointLight.shadow.camera.far = 25;
    var ambientLight = new THREE.AmbientLight(0xffffff, 2.0);
    ambientLight.shadowDarkness = 0.5;
    ambientLight.shadowCameraVisible = true;
    scene.add(ambientLight);
    scene.add(pointLight);
    var yLinhaProdInit = 20.5;
    var linhas = [];
    var yMaquina = 23.5;
    for (var i = 0; i < linhasProducao.length; i++) {
        drawLinhaProducao(yLinhaProdInit);
        var xMaquina = 10.5;
        for (var index = 0; index < linhasProducao[i].sequenciaMaquinas.length; index++) {
            drawMaquina(xMaquina, yMaquina, linhasProducao[i].sequenciaMaquinas[index].nome);
            xMaquina = xMaquina - 6;
        }
        yMaquina = yMaquina - 10;
        yLinhaProdInit = yLinhaProdInit - 10;
    }
}

//---------------------------------------------------- GUI ---------------------------------------------------------

/**
 * Construir GUI com todos os controlos possÃ­veis
 */
export function buildGUI() {
    let gui = new GUI({ width: 500 });

    let options = {
        addLinhaProducao: function () {
            //Cria uma copia do newLinhaProducao ao passar a Json e voltar a passar a obj
            //(senao as referencias das novas linhas seriam as mesmas)
            let linhaProducao = JSON.parse(JSON.stringify(newLinhaProducao));
            //VERIFICAÃ‡Ã•ES
            let canCreate = true;
            //Verificar se id nÃ£o Ã© vazio
            if (linhaProducao.designacao === '') {
                canCreate = false;
                alert("NÃo foi inserido um id para a linha de produção!")
            }
            //Verificar se id inserido jÃ¡ existe
            for (let i = 0; i < linhasProducao.length; i++) {
                if (linhasProducao[i].designacao === linhaProducao.designacao) {
                    canCreate = false;
                    alert("Linha de produção " + linhaProducao.designacao + " já existe!");
                }
            }
            //Verificar se existe maquinas na linha de produÃ§Ã£o criada
            if (linhaProducao.sequenciaMaquinas.length === 0) {
                canCreate = false;
                alert(linhaProducao.designacao + " não tem máquinas adicionadas!");
            }
            if (canCreate) {
                //Criar linha de produÃ§Ã£o
                linhasProducao.push(linhaProducao);
                httpPost('https://mdf3hgrupo3.azurewebsites.net/api/LinhaProducao', linhaProducao)
                //Update da folder de mostrar Linhas
                this.updateGUI();
            }
        },
        addMaquina: function () {
            //Cria uma copia do newMaquina ao passar a Json e voltar a passar a obj
            //(senao as referencias das novas maquinas seriam as mesmas)
            let maquina = JSON.parse(JSON.stringify(newMaquina));
            //VERIFICAÃ‡Ã•ES
            let canCreate = true;
            //Verifica se existe algum parÃ¢metro vazio
            if (maquina.nome === '' || maquina.tipoMaquina === '') {
                canCreate = false;
                alert("Não foi inserido um dos parametros da nova maquina!")
            }
            //Verifica se o identificador jÃ¡ existe
            for (let i = 0; i < maquinas.length; i++) {
                if (maquinas[i].nome === maquina.nome) {
                    canCreate = false;
                    alert("Máquina " + maquina.nome + " Já existe!");
                }
            }
            //Verifica se tipoMaquina inserido existe
            let exists = false;
            for (let i = 0; i < tiposMaquina.length; i++) {
                if (tiposMaquina[i].designacao === maquina.tipo)
                    exists = true;
            }

            if (!exists) {
                canCreate = false;
                alert("Tipo de Máquina inserido não existe! Consulte a lista de Tipos de Máquina.")
            }

            for (let index = 0; index < tiposMaquina.length; index++) {
                if (maquina.tipo == tiposMaquina[index].designacao) {
                    maquina.tipo = tiposMaquina[index];
                }

            }
            if (canCreate) {
                //Criar maquina
                maquinas.push(maquina);
                //Fazer post
                httpPost('https://mdf3hgrupo3.azurewebsites.net/api/Maquina', maquina);
                //Update da folder de criar Linhas
                this.updateGUI();
            }
        },
        showLinhaProducao: function () {
            objsLinhas = [];
            objsLinhasPos = [];
            //updateGUI
            this.updateGUI();
            //Reset do cenario
            this.deleteAll();
            draw();
            //Mostrar linhas selecionadas
            mostrarLinhasTemp = mostrarLinhas;
            mostrarLinhas = [];
        },
        fabricarProduto: function () {
            //Verificar se os dados estão todos preenchidos
            if (fabricar.produto === '' || fabricar.linha === '') {
                alert("Não foi selecionado o produto ou a linha de produção!");
                return;
            }
            console.log(produtos);
            //GET OPERACOES FROM LINHA
            let operacoesMaquinas = [];
            let operacoesFabricar = [];
            let indLinha;
            let linha;
            let produto;
            let encomenda;
            linhasProducao.forEach(element => {
                if (element.designacao == fabricar.linha) {
                    linha = element;
                }
            });
            encomendasOrdenadas.forEach(element => {
                if (element.codigo == fabricar.produto) {
                    encomenda = element;
                }
            });
            produtos.forEach(element => {
                if (encomenda.produto == element.designacao) {
                    produto = element;
                }
            });
            console.log(produto);
            console.log(linha)
            console.log(maquinas1)
            linha.sequenciaMaquinas.forEach(element => {
                operacoesMaquinas.push(element.tipo.operacoes)
            });
            produto.planoFabrico.operacoes.forEach(element => {
                operacoesFabricar.push(element);
            });

            console.log("operacoes a fabricar: ", operacoesFabricar);
            console.log("operacoes da maquina: ", operacoesMaquinas);


            //Verificar se linha consegue fabricar o produto
            let maquinasUsadas = [];
            for (let index = 0; index < operacoesFabricar.length; index++) {
                for (let i = 0; i < operacoesMaquinas.length; i++) {
                    for (let j = 0; j < operacoesMaquinas[i].length; j++) {
                        if (operacoesFabricar[index].designacao == operacoesMaquinas[i][j].designacao) {
                            maquinasUsadas.push(linha.sequenciaMaquinas[i])
                            break;
                        }
                    }
                }
            }
            if (maquinasUsadas.length == operacoesFabricar.length) {
                construirProdutoCubo(maquinasUsadas);
            } else {
                alert("Linha de Produção selecionada não consegue fabricar o produto selecionado!");
                return;
            }
        },
        deleteAll: function () {
            //Eliminar TODAS as linhas e maquinas
            while (scene.children.length > 0) {
                scene.remove(scene.children[0]);
            }
        },
        updateGUI: function () {
            gui.removeFolder(criarLinhaProducao);
            criarLinhaProducao = gui.addFolder('Criar Linha de produção');
            criarLinhaProducaoGUI();
            gui.removeFolder(criarMaquina);
            criarMaquina = gui.addFolder('Criar Máquina');
            criarMaquinaGUI();
            gui.removeFolder(fabricarProduto);
            fabricarProduto = gui.addFolder('Fabricar Produto');
            fabricarProdutoGUI();
            gui.removeFolder(mostrarLinhaProducao);
            mostrarLinhaProducao = gui.addFolder('Mostrar Linhas de produção');
            showLinhasProducaoGUI();
        }
    };

    //GUI de criar linhas de produÃ§Ã£o
    let newLinhaProducao = { designacao: "", sequenciaMaquinas: [] };
    let criarLinhaProducao = gui.addFolder('Criar Linha de Produção');
    criarLinhaProducaoGUI();

    //GUI de criar mÃ¡quinas
    let newMaquina = { nome: "", tipo: "" };
    let criarMaquina = gui.addFolder('Criar Máquina');
    criarMaquinaGUI();

    //GUI de fabricar o produto
    let fabricar = { produto: "", linha: "" };
    let mostrarLinhasTemp = [];
    let fabricarProduto = gui.addFolder('Fabricar Produto');
    fabricarProdutoGUI();

    //GUI de mostrar linhas de produÃ§Ã£o
    let mostrarLinhaProducao = gui.addFolder('Mostrar Linhas de Produção');
    showLinhasProducaoGUI();

    /**
     * CRIAR GUI DE CRIAR LINHA DE PRODUÃ‡ÃƒO
     */
    function criarLinhaProducaoGUI() {
        //Criar chooseBoxField para cada linha de produÃ§Ã£o
        let parameters = [], auxLinha = [], ind = 0;
        //Criar textField para o identificador
        criarLinhaProducao.add(newLinhaProducao, 'designacao').name('Designacao').listen();
        //Criar chooseBoxField para cada maquina
        for (let i = 0; i < maquinas.length; i++) {
            if (!linhasProducaoTemMaquina(maquinas[i])) {
                //Inicialmente comeÃ§am todas a false
                let show = false;
                parameters.push({ show });
                auxLinha.push(criarLinhaProducao.add(parameters[ind], "show").name("Adicionar Máquina " + maquinas[i].nome));
                //Se houver modificaÃ§Ãµes numa qualquer chooseBox -> adicionar ou remover nas maquinasIds
                auxLinha[ind].onChange(function (jar) {
                    if (jar)
                        newLinhaProducao.sequenciaMaquinas.push(maquinas[i]);
                    else {
                        let ind = newLinhaProducao.sequenciaMaquinas.indexOf(maquinas[i].nome);
                        newLinhaProducao.sequenciaMaquinas.splice(ind, 1);
                    }
                });
                ind++;
            }
        }
        //BotÃ£o de adicionar linha de produÃ§Ã£o Ã  base de dados
        criarLinhaProducao.add(options, 'addLinhaProducao').name('Adicionar Linha de produção');
    }

    /**
     * CRIAR GUI DE CRIAR MAQUINA
     */
    function criarMaquinaGUI() {
        //Criar textFields para o identificador, marca, localizacao, modelo
        criarMaquina.add(newMaquina, 'nome').name('Nome').listen();
        let tiposMaquinaIds = [];
        for (let i = 0; i < tiposMaquina.length; i++)
            tiposMaquinaIds.push(tiposMaquina[i].designacao);
        criarMaquina.add(newMaquina, 'tipo', tiposMaquinaIds).name('Tipo de máquina');
        //BotÃ£o de adicionar linha de produÃ§Ã£o Ã  base de dados
        criarMaquina.add(options, 'addMaquina').name('Adicionar máquina');
    }

    /**
 * CRIAR GUI DE FABRICAR PRODUTO
 */
    function fabricarProdutoGUI() {
        //Selecionar produto
        let encomendasIds = [];
        for (let i = 0; i < encomendasOrdenadas.length; i++)
            encomendasIds[i] = encomendasOrdenadas[i].codigo;
        fabricarProduto.add(fabricar, 'produto', encomendasIds).name('Produto');
        //Selecionar linhas
        let linhasIds = [];
        for (let i = 0; i < linhasProducao.length; i++)
            linhasIds[i] = linhasProducao[i].designacao;
        fabricarProduto.add(fabricar, 'linha', linhasIds).name('Linha de Produção');

        //Botão para fabricar produto
        fabricarProduto.add(options, 'fabricarProduto').name('Fabricar Produto');
    }

    /**
     * CRIAR GUI DE MOSTRAR LINHAS DE PRODUÃ‡ÃƒO
     */
    function showLinhasProducaoGUI() {
        let parameters = [], auxLinha = [];
        for (let i = 0; i < linhasProducao.length; i++) {
            //inicialmente comeÃ§am todas a false
            let show = false;
            parameters.push({ show });
            auxLinha.push(mostrarLinhaProducao.add(parameters[i], "show").name("Mostrar Linha de produção " + linhasProducao[i].designacao));
            //Se houver modificaÃ§Ãµes numa qualquer chooseBox -> adicionar ou remover em mostrarLinhas
            auxLinha[i].onChange(function (jar) {
                if (jar)
                    mostrarLinhas.push(linhasProducao[i]);
                else {
                    let ind = mostrarLinhas.indexOf(linhasProducao[i]);
                    mostrarLinhas.splice(ind, 1);
                }
            });
        }
        //BotÃ£o de mostrar linhas de produÃ§Ã£o selecionadas
        mostrarLinhaProducao.add(options, 'showLinhaProducao').name('Mostrar Linhas de produção');
    }

    /**
 * Verifica se a maquina nao está inserido em nenhuma Linha de produção
 */
    function linhasProducaoTemMaquina(maquina) {
        for (let i = 0; i < linhasProducao.length; i++) {
            if (linhasProducao[i].sequenciaMaquinas.indexOf(maquina.id) !== -1) {
                return true;
            }
        }
        return false;
    }
}

function animate() {
    requestAnimationFrame(animate);

    renderer.render(scene, camera);

}

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

function httpPost(theUrl, body) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("POST", theUrl); // false for synchronous request
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    var x = JSON.stringify(body);
    xmlHttp.send(JSON.stringify(body));
    return xmlHttp.responseText;
}

function drawLinhaProducao(yLinhaProdInit) {
    var loader = new GLTFLoader();
    loader.load(
        // resource URL
        'assets/textures/simple_rubber_conveyor/scene.gltf',
        // called when the resource is loaded
        function (gltf) {
            gltf.scene.traverse(function (node) {
                if (node instanceof THREE.Mesh) { node.castShadow = true; }
            });
            gltf.scene.scale.set(3, 3, 15);
            gltf.scene.castShadow = true;
            gltf.scene.receiveShadow = true;
            gltf.scene.translateX(yLinhaProdInit);
            scene.add(gltf.scene);
        },
        // called while loading is progressing
        function (xhr) {

            console.log((xhr.loaded / xhr.total * 100) + '% loaded');

        },
        // called when loading has errors
        function (error) {

            console.log('An error happened');

        }
    );
}

var maquinas1 = [];
function drawMaquina(y, x, nome) {
    var loader = new GLTFLoader();
    loader.load(
        // resource URL
        'assets/textures/robot_hand_modeling/scene.gltf',
        // called when the resource is loaded
        function (gltf) {
            gltf.scene.traverse(function (node) {
                if (node instanceof THREE.Mesh) { node.castShadow = true; }
            });
            gltf.scene.scale.set(0.5, 0.5, 0.5);
            gltf.scene.castShadow = true;
            gltf.scene.receiveShadow = true;
            gltf.scene.translateX(x);
            gltf.scene.translateY(1.10);
            gltf.scene.translateZ(y);
            gltf.scene.rotateY(-Math.PI / 2);
            scene.add(gltf.scene);
            gltf.scene.userData = nome;
            maquinas1.push(gltf.scene);

            var control = new TransformControls(camera, renderer.domElement);
            control.showY = false;
            control.size = 0.2;
            control.addEventListener('change', function () { renderer.render(scene, camera) });
            control.addEventListener('dragging-changed', function (event) {
                controls.enabled = !event.value;
            });

            control.attach(gltf.scene);
            scene.add(control);
        },
        // called while loading is progressing
        function (xhr) {

            console.log((xhr.loaded / xhr.total * 100) + '% loaded');

        },
        // called when loading has errors
        function (error) {

            console.log(error);

        }
    );
}

// this will be 2D coordinates of the current mouse position, [0,0] is middle of the screen.
var mouse = new THREE.Vector2();

var latestMouseProjection; // this is the latest projection of the mouse on object (i.e. intersection with ray)
var hoveredObj; // this objects is hovered at the moment

// tooltip will not appear immediately. If object was hovered shortly,
// - the timer will be canceled and tooltip will not appear at all.
var tooltipDisplayTimeout;

// This will move tooltip to the current mouse position and show it by timer.
function showTooltip() {
    var divElement = $("#tooltip");

    if (divElement && latestMouseProjection) {
        divElement.css({
            display: "block",
            opacity: 0.0
        });

        var canvasHalfWidth = renderer.domElement.offsetWidth / 2;
        var canvasHalfHeight = renderer.domElement.offsetHeight / 2;

        var tooltipPosition = latestMouseProjection.clone().project(camera);
        tooltipPosition.x = (tooltipPosition.x * canvasHalfWidth) + canvasHalfWidth + renderer.domElement.offsetLeft;
        tooltipPosition.y = -(tooltipPosition.y * canvasHalfHeight) + canvasHalfHeight;

        //var tootipWidth = divElement[0].offsetWidth;
        //var tootipHeight = divElement[0].offsetHeight;

        divElement.css({
            left: `${tooltipPosition.x}px`,
            top: `${tooltipPosition.y}px`
        });

        // var position = new THREE.Vector3();
        // var quaternion = new THREE.Quaternion();
        // var scale = new THREE.Vector3();
        // hoveredObj.matrix.decompose(position, quaternion, scale);
        divElement.text(hoveredObj);
        console.log(hoveredObj);

        setTimeout(function () {
            divElement.css({
                opacity: 1.0
            });
        }, 25);
    }
}

// This will immediately hide tooltip.
function hideTooltip() {
    var divElement = $("#tooltip");
    if (divElement) {
        divElement.css({
            display: "none"
        });
    }
}

// Following two functions will convert mouse coordinates
// from screen to three.js system (where [0,0] is in the middle of the screen)
function updateMouseCoords(event, coordsObj) {
    coordsObj.x = (event.clientX / window.innerWidth) * 2 - 1;
    coordsObj.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

// CONSTRUIR PRODUTO ------------------------------------

/**
 * Constroi o produto inicial - cubo
 * @param indLinha indice da linha de produção no array objects e mostrarLinhas
 * @param maquinasUsadas lista de maquinas usadas para criar este produto
 * @param indiceProduto indice do produto no array produtos e produtosModels
 * @param nomeProduto string com o id do produto
 * @param operacoes string com os ids das operações necessárias
 */
function construirProdutoCubo(maquinasUsadas) {
    for (let i = 0; i < maquinasUsadas.length; i++) {
        for (let j = 0; j < maquinas1.length; j++) {
            var time;
            if (i ==0) {
                time = 0;
            }
            else{
                time = 6000;
            }
            setTimeout(() => {
                if (maquinas1[j].userData == maquinasUsadas[i].nome) {
                    let geometry = new THREE.BoxGeometry(1, 1, 1);
                    let newColor = (Math.random() * 0xFFFFFF << 0);
                    //let material = new THREE.MeshBasicMaterial({ color:  0xFFFFFF });
                    let material = new THREE.MeshBasicMaterial({ color: newColor });
                    material.color.setHex(Math.random() * 0xffffff);
                    let cube = new THREE.Mesh(geometry, material);
                    cube.position.x = maquinas1[j].position.x - 3;
                    cube.position.y = maquinas1[j].position.y + 1.5;
                    cube.position.z = maquinas1[j].position.z;
                    cube.receiveShadow = true;
                    cube.castShadow = true;
                    scene.add(cube);

                    animate();
                    setTimeout(() => {
                        scene.remove(scene.children[scene.children.length - 1]);
                        animate();
                    }, 5000);
                }
            }, i * 6000);
        }
    }

}

function sleep(miliseconds) {
    var currentTime = new Date().getTime();
 
    while (currentTime + miliseconds >= new Date().getTime()) {
    }
 }


/**
 * Update do produto(cubo) para um modelo
 * @param pai objeto pai do produto (linhaProducao)
 * @param x coordenada Ox onde o produto está a ser construido
 * @param i indice do produto
 * @param model modelo do produto
 */
function construirProduto(pai, x, i, model) {
    let loader = new GLTFLoader();
    loader.load(model, function (gltf) {
        //Para criar sombras
        gltf.scene.traverse(function (node) {
            if (node instanceof THREE.Mesh) { node.castShadow = true; }
        });

        let produto = gltf.scene;
        produto.rotateY(-45);
        produto.castShadow = true;
        produto.receiveShadow = true;

        produto.position.x = x + 0.001;

        //MUG
        if (model.includes("mug")) {
            produto.scale.set(0.02, 0.02, 0.02);
            produto.position.y = 0.012;
        }
        //FORK AND SPOON
        if (model.includes("fork") || model.includes("spoon")) {
            produto.scale.set(0.01, 0.01, 0.01);
            produto.position.y = 0.012;
        }
        //PLATE
        //FORK AND SPOON
        if (model.includes("plate")) {
            produto.scale.set(0.008, 0.008, 0.008);
            produto.position.y = 0.012;
        }

        produto.children[0].children[0].userData.tooltipText = objsProdutos[i].userData.tooltipText;
        tooltipEnabledObjects.push(produto);

        pai.add(produto);
        pai.remove(objsProdutos[i]);
        objsProdutos[i] = produto;
        parado[i] = false;
    });
}

function handleManipulationUpdate() {
    var raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(mouse, camera);
    {
        var intersects = raycaster.intersectObjects(maquinas1, true);
        if (intersects.length > 0) {
            latestMouseProjection = intersects[0].point;
            hoveredObj = intersects[0].object.parent.parent.parent.parent.userData;
        }
    }

    if (tooltipDisplayTimeout || !latestMouseProjection) {
        clearTimeout(tooltipDisplayTimeout);
        tooltipDisplayTimeout = undefined;
        hideTooltip();
    }

    if (!tooltipDisplayTimeout && latestMouseProjection) {
        tooltipDisplayTimeout = setTimeout(function () {
            tooltipDisplayTimeout = undefined;
            showTooltip();
        }, 330);
    }
}

function onMouseMove(event) {
    updateMouseCoords(event, mouse);

    latestMouseProjection = undefined;
    hoveredObj = undefined;
    handleManipulationUpdate();
}
