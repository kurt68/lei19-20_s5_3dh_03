function drawLinhaProducao(y, x) {
	var mtlLoader = new THREE.MTLLoader();
	mtlLoader.load("textures/scene.mtl", function (materials) {
		materials.preload(); var objLoader = new THREE.OBJLoader();
		objLoader.crossOrigins = '';
		objLoader.load("textures/scene.obj", function (object) {
			scene.add(object);
			object.scale.set(0.004, 0.016, 0.003);
			object.position.y = 0;
			object.position.x = x;
			object.position.z = 0;
			object.rotateX(-Math.PI / 2);
		});
	});
}