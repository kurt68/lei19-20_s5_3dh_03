import { Component, OnInit } from '@angular/core';
import { TipoMaquinaService } from '../service/tipo-maquina.service';
import { TipoMaquina } from '../model/tipoMaquina';
import { Operacao } from '../model/operacao';
import { OperacaoService } from '../service/operacao.service';

@Component({
  selector: 'app-put-tipo-maquina',
  templateUrl: './put-tipo-maquina.component.html',
  styleUrls: ['./put-tipo-maquina.component.css']
})
export class PutTipoMaquinaComponent implements OnInit {

  tiposMaquinas: Array<TipoMaquina>;
  selected: TipoMaquina;

  tipoMaquina: TipoMaquina;
  listaOperacoes: Array<Operacao>;
  operacoesEscolhidas: Array<Operacao>;
  operacoesNEscolhidas: Array<Operacao>;

  flag: boolean = false;

  constructor(private service: TipoMaquinaService, private opService: OperacaoService) { }

  ngOnInit() {
    this.getTiposMaquinas();
  }

  getTiposMaquinas(): void {
    this.service.getIds().subscribe(data => {
      this.tiposMaquinas = data;
    })
  }

  getTipo() {

    this.operacoesEscolhidas = this.selected.operacoes;
    this.opService.getIds().subscribe(data => {
      this.listaOperacoes = data;
    })
    this.flag = true;

  }

  getTipo2() {
     this.operacoesNEscolhidas = this.listaOperacoes.filter(item => this.operacoesEscolhidas.indexOf(item) < 0);
  }

  putTipoMaquina(){
    this.selected.operacoes = this.operacoesEscolhidas;
    this.service.putTipoMaquina(this.selected, this.selected.designacao).subscribe(
      (res : any) => {
        console.log(res);
      },
      (err : any) => {
        console.log(err);
      }
    );
  }

}
