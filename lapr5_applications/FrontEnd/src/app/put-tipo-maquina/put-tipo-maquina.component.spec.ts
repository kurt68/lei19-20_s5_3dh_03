import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutTipoMaquinaComponent } from './put-tipo-maquina.component';

describe('PutTipoMaquinaComponent', () => {
  let component: PutTipoMaquinaComponent;
  let fixture: ComponentFixture<PutTipoMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutTipoMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutTipoMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
