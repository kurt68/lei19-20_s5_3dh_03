import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutOperacaoComponent } from './put-operacao.component';

describe('PutOperacaoComponent', () => {
  let component: PutOperacaoComponent;
  let fixture: ComponentFixture<PutOperacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutOperacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutOperacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
