import { Component, OnInit } from '@angular/core';
import { Encomenda } from '../model/encomenda';
import { AuthenticationService } from '../service/authentication.service';
import { EncomendaService } from '../service/encomenda.service';

@Component({
  selector: 'app-get-encomenda-admin',
  templateUrl: './get-encomenda-admin.component.html',
  styleUrls: ['./get-encomenda-admin.component.css']
})
export class GetEncomendaAdminComponent implements OnInit {

  flag: boolean;
  encomendas: Array<Encomenda>;

  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService) { }

  ngOnInit() {
    this.getEncomendas();
  }

  getEncomendas(): void {
    this.encomendaService.getEncomendas().subscribe(data => {
      this.encomendas = data;
    })
  }

  getEncomenda(): void {
    this.flag = true;
  }

}
