import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetEncomendaAdminComponent } from './get-encomenda-admin.component';

describe('GetEncomendaAdminComponent', () => {
  let component: GetEncomendaAdminComponent;
  let fixture: ComponentFixture<GetEncomendaAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetEncomendaAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetEncomendaAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
