import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AuthenticationService } from '../service/authentication.service';
import { EncomendaService } from '../service/encomenda.service';
import { Encomenda } from '../model/encomenda';
import { PlanoProducaoService } from '../service/plano-producao.service'


@Component({
  selector: 'app-ordenar-encomendas',
  templateUrl: './ordenar-encomendas.component.html',
  styleUrls: ['./ordenar-encomendas.component.css']
})
export class OrdenarEncomendasComponent implements OnInit {

  encomendas: Array<any>;
  encomendasValidas: Array<Encomenda>;

  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService, private planoService:PlanoProducaoService) { }

  ngOnInit() {
    this.getEncomendas();
  }

  getEncomendas(): void {
    this.planoService.getPlanoProducao().subscribe(data => {
      this.encomendas = data[0].listaEncomendas;
      console.log(data)
    })
  }

  ngOnDestroy() {
    this.planoService.putPlanoProducao(this.encomendas).subscribe(
      (res: any) => {
        console.log(res);
      },
      (err: any) => {
        console.log(err);
      }
    );
  }  

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.encomendas, event.previousIndex, event.currentIndex);
  }
}
