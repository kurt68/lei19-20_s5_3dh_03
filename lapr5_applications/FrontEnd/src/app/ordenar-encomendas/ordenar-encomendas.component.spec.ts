import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenarEncomendasComponent } from './ordenar-encomendas.component';

describe('OrdenarEncomendasComponent', () => {
  let component: OrdenarEncomendasComponent;
  let fixture: ComponentFixture<OrdenarEncomendasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdenarEncomendasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdenarEncomendasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
