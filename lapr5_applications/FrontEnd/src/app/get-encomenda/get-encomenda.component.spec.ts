import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetEncomendaComponent } from './get-encomenda.component';

describe('GetEncomendaComponent', () => {
  let component: GetEncomendaComponent;
  let fixture: ComponentFixture<GetEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
