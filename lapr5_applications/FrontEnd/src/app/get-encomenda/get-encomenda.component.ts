import { Component, OnInit } from '@angular/core';
import { EncomendaService } from '../service/encomenda.service';
import { AuthenticationService } from '../service/authentication.service';
import { Encomenda } from '../model/encomenda';

@Component({
  selector: 'app-get-encomenda',
  templateUrl: './get-encomenda.component.html',
  styleUrls: ['./get-encomenda.component.css']
})
export class GetEncomendaComponent implements OnInit {

  flag: boolean;
  encomendas: Array<Encomenda>;

  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService) { }

  ngOnInit() {
    this.getEncomendas();
  }

  getEncomendas(): void {
    this.encomendaService.getEncomendas().subscribe(data => {
      this.encomendas = data;
    })
  }

  getEncomenda(): void {
    this.flag = true;
  }

}
