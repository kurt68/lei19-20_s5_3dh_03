import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-registar',
  templateUrl: './registar.component.html',
  styleUrls: ['./registar.component.css']
})
export class RegistarComponent implements OnInit {

  username: string;
  nome: string;
  password: string;
  email: string;

  constructor(private autenticacao: AuthenticationService, private router: Router, private _snackBar: MatSnackBar) { }


  ngOnInit() {
  }

  registar(): void {
    console.log(this.email);
    this.autenticacao.UtilizadorSignUp(this.nome, this.username, this.email, this.password).subscribe(
      (res : any) => {
        console.log(res);
        this.router.navigate(['']);
        this._snackBar.open("Utilizador criado com sucesso!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("utilizador invalido!", "", {
          duration: 4500,
        });
      }
    );

  }



}
