import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostTipoMaquinaComponent } from './post-tipo-maquina.component';

describe('PostTipoMaquinaComponent', () => {
  let component: PostTipoMaquinaComponent;
  let fixture: ComponentFixture<PostTipoMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostTipoMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostTipoMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
