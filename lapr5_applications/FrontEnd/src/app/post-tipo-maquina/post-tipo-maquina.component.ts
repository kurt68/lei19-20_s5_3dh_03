import { Component, OnInit } from '@angular/core';
import { OperacaoService } from '../service/operacao.service';
import { Operacao } from '../model/operacao';
import { FormControl } from '@angular/forms';
import { TipoMaquinaService } from '../service/tipo-maquina.service';
import { TipoMaquina } from '../model/tipoMaquina';

@Component({
  selector: 'app-post-tipo-maquina',
  templateUrl: './post-tipo-maquina.component.html',
  styleUrls: ['./post-tipo-maquina.component.css']
})

export class PostTipoMaquinaComponent implements OnInit {

  listaOperacoes: Array<Operacao>;
  designacao: string;
  operacoesEscolhidas: Array<Operacao>;
  tipoMaquina: TipoMaquina


constructor(private opService: OperacaoService, private tmService : TipoMaquinaService) { }

ngOnInit() {
  this.getOperacoes();
}

getOperacoes(): void {
  this.opService.getIds().subscribe(data => {
    this.listaOperacoes = data;
  })
}

postTipoMaquina(){
  this.tipoMaquina = new TipoMaquina(this.designacao,this.operacoesEscolhidas);
  this.tmService.postTipoMaquina(this.tipoMaquina).subscribe(
    (res : any) => {
      console.log(res);
    },
    (err : any) => {
      console.log(err);
    }
  );
}

}
