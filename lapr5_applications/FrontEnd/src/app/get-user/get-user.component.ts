import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { Utilizador } from '../model/utilizador';

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {

  user: Utilizador;
  flag: boolean = false;

  constructor(private router: Router, private service: UserService) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void{
    this.service.getUser().subscribe(data => {
      this.user = data.user;
    })
  }

}
