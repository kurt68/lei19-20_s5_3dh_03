import { Component, OnInit } from '@angular/core';
import { Encomenda } from '../model/encomenda';
import { AuthenticationService } from '../service/authentication.service';
import { EncomendaService } from '../service/encomenda.service';
import { ProdutoService } from '../service/produto.service';
import { Produto } from '../model/produto';

@Component({
  selector: 'app-put-encomenda',
  templateUrl: './put-encomenda.component.html',
  styleUrls: ['./put-encomenda.component.css']
})
export class PutEncomendaComponent implements OnInit {

  flag: boolean;
  encomendas: Array<Encomenda>;
  selected: Encomenda;
  produtos: Array<Produto>;
  morada: string;
  quantidade: number;
  id: string;
  produto: Produto;
  encomenda: Encomenda;
  estado: string;

  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService, private produtoService: ProdutoService) { }

  ngOnInit() {
    this.getEncomendas();
    this.getProdutos();
  }

  getEncomendas(): void {
    this.encomendaService.getEncomendas().subscribe(data => {
      this.encomendas = data;
    })
  }

  getProdutos(): void {
    this.produtoService.getIds().subscribe(data => {
      this.produtos = data;
    })
  }

  getEncomenda(): void {
    this.flag = true;
    this.encomendaService.getEncomendas().subscribe(data => {
      data.forEach(element => {
        if (element.codigo == this.selected.codigo) {
          this.id = element._id;
        }
      })
    })
    
    console.log(this.id)

  }

  putEncomenda(): void {
    this.encomendaService.putEncomenda(this.estado, this.produto, this.quantidade, this.morada, this.id).subscribe(
      (res: any) => {
        console.log(res);
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
}
