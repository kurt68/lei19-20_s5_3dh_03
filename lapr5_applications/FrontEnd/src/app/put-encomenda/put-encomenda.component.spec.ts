import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutEncomendaComponent } from './put-encomenda.component';

describe('PutEncomendaComponent', () => {
  let component: PutEncomendaComponent;
  let fixture: ComponentFixture<PutEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
