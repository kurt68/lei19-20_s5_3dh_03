import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VizCanvasComponent } from './viz-canvas/viz-canvas.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { HomeComponent } from './home/home.component';
import { InitialComponent } from './initial/initial.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PostOperacaoComponent } from './post-operacao/post-operacao.component';
import { GetOperacaoComponent } from './get-operacao/get-operacao.component';
import { PutOperacaoComponent } from './put-operacao/put-operacao.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { GetMaquinaComponent } from './get-maquina/get-maquina.component';
import { GetTipoMaquinaComponent } from './get-tipo-maquina/get-tipo-maquina.component';
import { GetProdutoComponent } from './get-produto/get-produto.component';
import { PostTipoMaquinaComponent } from './post-tipo-maquina/post-tipo-maquina.component';
import { PutTipoMaquinaComponent } from './put-tipo-maquina/put-tipo-maquina.component';
import { GetLinhaProducaoComponent } from './get-linha-producao/get-linha-producao.component';
import { PostMaquinaComponent } from './post-maquina/post-maquina.component';
import { PostProdutoComponent } from './post-produto/post-produto.component';
import { PutMaquinaComponent } from './put-maquina/put-maquina.component';
import { PostLinhaProducaoComponent } from './post-linha-producao/post-linha-producao.component';
import { RegistarComponent } from './registar/registar.component';
import { LoginComponent } from './login/login.component';
import { PostEncomendaComponent } from './post-encomenda/post-encomenda.component';
import { GetEncomendaComponent } from './get-encomenda/get-encomenda.component';
import { GetUserComponent } from './get-user/get-user.component';
import { PutUserComponent } from './put-user/put-user.component';
import { GetEncomendaAdminComponent } from './get-encomenda-admin/get-encomenda-admin.component';
import { PutEncomendaComponent } from './put-encomenda/put-encomenda.component';
import { CancelarEncomendaComponent } from './cancelar-encomenda/cancelar-encomenda.component';
import { GetUsersAdminComponent } from './get-users-admin/get-users-admin.component';
import { TermosComponent } from './termos/termos.component';
import { OrdenarEncomendasComponent } from './ordenar-encomendas/ordenar-encomendas.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AtivarMaquinaComponent } from './ativar-maquina/ativar-maquina.component'




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VizCanvasComponent,
    InitialComponent,
    PostOperacaoComponent,
    GetOperacaoComponent,
    PutOperacaoComponent,
    GetMaquinaComponent,
    GetTipoMaquinaComponent,
    GetProdutoComponent,
    PostTipoMaquinaComponent,
    PutTipoMaquinaComponent,
    GetLinhaProducaoComponent,
    PostMaquinaComponent,
    PostProdutoComponent,
    PutMaquinaComponent,
    PostLinhaProducaoComponent,
    RegistarComponent,
    LoginComponent,
    PostEncomendaComponent,
    GetEncomendaComponent,
    GetUserComponent,
    PutUserComponent,
    GetEncomendaAdminComponent,
    PutEncomendaComponent,
    CancelarEncomendaComponent,
    GetUsersAdminComponent,
    TermosComponent,
    OrdenarEncomendasComponent,
    AtivarMaquinaComponent
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSnackBarModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
