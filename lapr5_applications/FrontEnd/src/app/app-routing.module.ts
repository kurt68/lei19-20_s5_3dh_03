import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InitialComponent } from './initial/initial.component';
import { GetOperacaoComponent } from './get-operacao/get-operacao.component';
import { GetMaquinaComponent } from './get-maquina/get-maquina.component';
import { GetTipoMaquinaComponent } from './get-tipo-maquina/get-tipo-maquina.component';
import { GetProdutoComponent } from './get-produto/get-produto.component';
import { PostTipoMaquinaComponent } from './post-tipo-maquina/post-tipo-maquina.component';
import { PutTipoMaquinaComponent } from './put-tipo-maquina/put-tipo-maquina.component';
import { PostOperacaoComponent } from './post-operacao/post-operacao.component';
import { GetLinhaProducaoComponent } from './get-linha-producao/get-linha-producao.component';
import { PostMaquinaComponent } from './post-maquina/post-maquina.component';
import { PostProdutoComponent } from './post-produto/post-produto.component';
import { PutMaquinaComponent } from './put-maquina/put-maquina.component';
import { PostLinhaProducaoComponent } from './post-linha-producao/post-linha-producao.component';
import { LoginComponent } from './login/login.component';
import { RegistarComponent } from './registar/registar.component';
import { PostEncomendaComponent } from './post-encomenda/post-encomenda.component';
import { GetEncomendaComponent } from './get-encomenda/get-encomenda.component';
import { GetUserComponent } from './get-user/get-user.component';
import { PutUserComponent } from './put-user/put-user.component';
import {GetEncomendaAdminComponent} from './get-encomenda-admin/get-encomenda-admin.component';
import {PutEncomendaComponent} from './put-encomenda/put-encomenda.component';
import {CancelarEncomendaComponent} from './cancelar-encomenda/cancelar-encomenda.component';
import { GetUsersAdminComponent } from './get-users-admin/get-users-admin.component';
import { TermosComponent } from './termos/termos.component';
import { OrdenarEncomendasComponent } from './ordenar-encomendas/ordenar-encomendas.component';
import { AtivarMaquinaComponent} from './ativar-maquina/ativar-maquina.component';



const routes: Routes = [
  { path: '', component: InitialComponent },
  { path: 'home', component: HomeComponent },
  { path: 'signup', component: RegistarComponent },
  { path: 'login', component: LoginComponent },
  { path: 'operacao/get', component: GetOperacaoComponent },
  { path: 'operacao/post', component: PostOperacaoComponent },
  { path: 'maquina/get', component: GetMaquinaComponent },
  { path: 'tipoMaquina/get', component: GetTipoMaquinaComponent },
  { path: 'produto/get', component: GetProdutoComponent },
  { path: 'tipoMaquina/post', component: PostTipoMaquinaComponent },
  { path: 'tipoMaquina/put', component: PutTipoMaquinaComponent },
  { path: 'linhaProducao/get', component: GetLinhaProducaoComponent },
  { path: 'maquina/post', component: PostMaquinaComponent },
  { path: 'produto/post', component: PostProdutoComponent },
  { path: 'maquina/put', component: PutMaquinaComponent },
  { path: 'linhaProducao/post', component: PostLinhaProducaoComponent },
  { path: 'encomenda/post', component: PostEncomendaComponent },
  { path: 'encomenda/get', component: GetEncomendaComponent },
  { path: 'user/get', component: GetUserComponent },
  { path: 'user/put', component: PutUserComponent },
  {path: 'encomendaAdmin/get', component: GetEncomendaAdminComponent},
  {path: 'encomenda/put', component: PutEncomendaComponent},
  {path: 'encomenda/cancelar', component: CancelarEncomendaComponent},
  {path: 'user/getAll', component: GetUsersAdminComponent},
  {path: 'termos', component: TermosComponent},
  {path: 'ordenarEncomendas', component: OrdenarEncomendasComponent},
  {path: 'ativarMaquina', component: AtivarMaquinaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
