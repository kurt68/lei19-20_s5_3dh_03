import { Component, OnInit } from '@angular/core';
import { MaquinaService } from '../service/maquina.service';
import { TipoMaquinaService } from '../service/tipo-maquina.service';
import { Maquina } from '../model/maquina';
import { TipoMaquina } from '../model/tipoMaquina';

@Component({
  selector: 'app-put-maquina',
  templateUrl: './put-maquina.component.html',
  styleUrls: ['./put-maquina.component.css']
})
export class PutMaquinaComponent implements OnInit {

  maquinas: Array<Maquina>;
  selected: Maquina;

  tipoEscolhido: TipoMaquina;
  flag: boolean = false;
  listaTipos: Array<TipoMaquina>;

  constructor(private service: MaquinaService, private tmService: TipoMaquinaService) { }

  ngOnInit() {
    this.getMaquinas();
  }

  getMaquinas(): void {
    this.service.getIds().subscribe(data => {
      this.maquinas = data;
    })
  }

  getMaquina() {

    this.tipoEscolhido = this.selected.tipo;
    this.tmService.getIds().subscribe(data => {
      this.listaTipos = data;
    })
    this.flag = true;

  }

  putMaquina(){
    this.selected.tipo = this.tipoEscolhido;
    this.service.putMaquina(this.selected, this.selected.nome).subscribe(
      (res : any) => {
        console.log(res);
      },
      (err : any) => {
        console.log(err);
      }
    );
  }

}
