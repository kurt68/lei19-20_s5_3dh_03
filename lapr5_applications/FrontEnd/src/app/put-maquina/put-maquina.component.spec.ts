import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PutMaquinaComponent } from './put-maquina.component';

describe('PutMaquinaComponent', () => {
  let component: PutMaquinaComponent;
  let fixture: ComponentFixture<PutMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PutMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PutMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
