import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetTipoMaquinaComponent } from './get-tipo-maquina.component';

describe('GetTipoMaquinaComponent', () => {
  let component: GetTipoMaquinaComponent;
  let fixture: ComponentFixture<GetTipoMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetTipoMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetTipoMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
