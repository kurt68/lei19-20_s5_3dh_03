import { Component, OnInit } from '@angular/core';
import { TipoMaquina } from '../model/tipoMaquina';
import { Operacao } from '../model/operacao';
import { TipoMaquinaService } from '../service/tipo-maquina.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Maquina } from '../model/maquina';

export interface DialogData {
  designacao: string;
  operacoes: Array<string>;
}

@Component({
  selector: 'app-get-tipo-maquina',
  templateUrl: './get-tipo-maquina.component.html',
  styleUrls: ['./get-tipo-maquina.component.css']
})
export class GetTipoMaquinaComponent implements OnInit {

  tiposMaquinas: Array<TipoMaquina>;
  selected: TipoMaquina;

  designacao: string;
  operacoes: Array<Operacao>;
  maquinas: Array<Maquina>;

  flag: boolean = false;

  constructor(private router: Router, private service: TipoMaquinaService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getTiposMaquinas();
  }
  getTiposMaquinas(): void {
    this.service.getIds().subscribe(data => {
      this.tiposMaquinas = data;
    })
  }

  getTipo() {
    this.operacoes = this.selected.operacoes
    this.flag = true;
    this.getMaquinas();
  }
  getMaquinas() {
    this.service.getMaquinas(this.selected.designacao).subscribe(data => {
      this.maquinas = data;
    })
  }
}
