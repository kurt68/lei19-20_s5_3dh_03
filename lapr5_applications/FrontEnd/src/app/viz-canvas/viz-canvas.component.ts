import { Component, OnInit, HostListener } from '@angular/core';

//import { init } from '../../assets/js/main.js';
import { init } from '../../assets/js/test.js';
import { buildGUI } from '../../assets/js/test.js';
import { Router } from '@angular/router';
import { MaquinaService } from '../service/maquina.service.js';
import { Maquina } from '../model/maquina.js';
import { TipoMaquina } from '../model/tipoMaquina.js';
import { LinhaProducao } from '../model/linhaProducao';
import { TipoMaquinaService } from '../service/tipo-maquina.service.js';
import { LinhaProducaoService } from '../service/linha-producao.service.js';
import { PlanoProducaoService } from "../service/plano-producao.service";
import { Encomenda } from "../model/encomenda";

@Component({
  selector: 'app-viz-canvas',
  templateUrl: './viz-canvas.component.html',
  styleUrls: ['./viz-canvas.component.css']
})

export class VizCanvasComponent implements OnInit {

  maquinas: Array<Maquina>;
  tiposMaquinas: Array<TipoMaquina>;
  linhasProducao: Array<LinhaProducao>;
  encomendas: any;

  constructor(private router: Router, private maquinaservice: MaquinaService, private tipoService: TipoMaquinaService, private linhaService: LinhaProducaoService, private planoService : PlanoProducaoService) { }

  ngOnInit() {
    this.getPlanoProduto();
  }

  getTiposMaquinas(): void {
    this.tipoService.getIds().subscribe(data => {
      this.tiposMaquinas = data;
    })
  }

  getMaquinas(): void{
    this.maquinaservice.getIds().subscribe(data => {
      this.maquinas = data;
    })
  }

  getPlanoProduto(): void{
    this.planoService.getPlanoProducao().subscribe(data => {
      this.encomendas = data;
      console.log(this.encomendas[0].listaEncomendas)
      init(this.encomendas[0].listaEncomendas);
    })
  }  

  @HostListener('window:beforeunload')
  ngOnDestroy() {
    window.location.reload();
  }

}