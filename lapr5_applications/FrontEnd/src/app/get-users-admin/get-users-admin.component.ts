import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Utilizador } from '../model/utilizador';

@Component({
  selector: 'app-get-users-admin',
  templateUrl: './get-users-admin.component.html',
  styleUrls: ['./get-users-admin.component.css']
})
export class GetUsersAdminComponent implements OnInit {

  clientes: Array<String>;
  selected: Utilizador;
  flag: boolean;


  constructor(private service: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void{
    this.service.getUsers().subscribe(data => {
      this.clientes = data.user;
    })
  }

  guardarPrioridades(): void{

  }

}
