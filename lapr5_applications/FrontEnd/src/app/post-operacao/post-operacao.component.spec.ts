import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostOperacaoComponent } from './post-operacao.component';

describe('PostOperacaoComponent', () => {
  let component: PostOperacaoComponent;
  let fixture: ComponentFixture<PostOperacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostOperacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostOperacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
