import { Component, OnInit } from '@angular/core';
import { OperacaoService } from '../service/operacao.service';
import { Operacao } from '../model/operacao';

@Component({
  selector: 'app-post-operacao',
  templateUrl: './post-operacao.component.html',
  styleUrls: ['./post-operacao.component.css']
})
export class PostOperacaoComponent implements OnInit {

  designacao: string;
  ferramenta: string;
  duracao: Float32Array;
  operacao: Operacao;

  constructor(private opService: OperacaoService) { }

  ngOnInit() {
  }

  postOperacoes(){
    this.operacao = new Operacao(this.designacao, this.ferramenta, this.duracao);
    this.opService.postOperacao(this.operacao).subscribe(
      (res : any) => {
        console.log(res);
      },
      (err : any) => {
        console.log(err);
      }
    );
  }

}
