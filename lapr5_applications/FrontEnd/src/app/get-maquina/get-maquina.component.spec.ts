import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetMaquinaComponent } from './get-maquina.component';

describe('GetMaquinaComponent', () => {
  let component: GetMaquinaComponent;
  let fixture: ComponentFixture<GetMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
