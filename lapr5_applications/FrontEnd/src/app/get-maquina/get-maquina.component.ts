import { Component, OnInit } from '@angular/core';
import { Maquina } from '../model/maquina';
import { MaquinaService } from '../service/maquina.service';
import { Router } from '@angular/router';
import { TipoMaquina } from '../model/tipoMaquina';

@Component({
  selector: 'app-get-maquina',
  templateUrl: './get-maquina.component.html',
  styleUrls: ['./get-maquina.component.css']
})
export class GetMaquinaComponent implements OnInit {

  maquinas: Array<Maquina>;
  selected: Maquina;

  designacao: string;
  tiposMaquinas: TipoMaquina;

  flag: boolean = false;
  
  constructor(private router: Router, private service: MaquinaService) { }

  ngOnInit() {
    this.getMaquinas();
  }
  getMaquinas(): void{
    this.service.getIds().subscribe(data => {
      this.maquinas = data;
    })
  }

  getMaquina(){
    this.flag=true;
    this.tiposMaquinas = this.selected.tipo;
    
  }
  

}
