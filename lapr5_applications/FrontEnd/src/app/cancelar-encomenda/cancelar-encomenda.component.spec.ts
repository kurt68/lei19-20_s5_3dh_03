import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelarEncomendaComponent } from './cancelar-encomenda.component';

describe('CancelarEncomendaComponent', () => {
  let component: CancelarEncomendaComponent;
  let fixture: ComponentFixture<CancelarEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelarEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelarEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
