import { Component, OnInit } from '@angular/core';
import { EncomendaService } from '../service/encomenda.service';
import { Encomenda } from '../model/encomenda';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-cancelar-encomenda',
  templateUrl: './cancelar-encomenda.component.html',
  styleUrls: ['./cancelar-encomenda.component.css']
})
export class CancelarEncomendaComponent implements OnInit {

  flag: boolean;
  encomendas: Array<Encomenda>;
  id: string;
  selected: Encomenda;
  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getEncomendas();
  }

  getEncomendas(): void {
    this.encomendaService.getEncomendas().subscribe(data => {
      this.encomendas = data;
    })
  }

  getEncomenda(){
    this.flag=true;
    this.encomendaService.getEncomendas().subscribe(data => {
      data.forEach(element => {
        if (element.codigo == this.selected.codigo) {
          this.id = element._id;
        }
      })
    })
  }

  cancelarEncomenda() {

    this.encomendaService.cancelarEncomenda(this.id).subscribe(
      (res : any) => {
        console.log(res);
        this._snackBar.open("Encomenda Cancelada com sucesso!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("Erro a cancelar encomenda!", "", {
          duration: 4500,
        });
      }
    );;

  }

}
