import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-initial',
  templateUrl: './initial.component.html',
  styleUrls: ['./initial.component.css'],
  encapsulation: ViewEncapsulation.Native,
})
export class InitialComponent implements OnInit {


  constructor() { }

  ngOnInit() {
  }

}
