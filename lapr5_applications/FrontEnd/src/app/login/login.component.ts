import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;

  constructor(private autenticacao: AuthenticationService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  login(): void{
    this.autenticacao.UtilizadorLogin(this.username,this.password).subscribe(info =>{
      this.autenticacao.loginSucceeded(info);
      this.router.navigate([""]);
    },
    (err : any) => {
      console.log(err);
      this._snackBar.open("Username ou password invalido!", "", {
        duration: 4500,
      });
    }
    );
  }

}
