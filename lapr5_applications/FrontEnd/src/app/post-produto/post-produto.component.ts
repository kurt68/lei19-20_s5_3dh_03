import { Component, OnInit } from '@angular/core';
import { PlanoFabrico } from '../Model/PlanoFabrico';
import { Operacao } from '../Model/Operacao';
import { Produto } from '../model/produto';
import { OperacaoService } from '../service/operacao.service';
import { ProdutoService } from '../service/produto.service';

@Component({
  selector: 'app-post-produto',
  templateUrl: './post-produto.component.html',
  styleUrls: ['./post-produto.component.css']
})
export class PostProdutoComponent implements OnInit {

  designacao: string;
  selected: Produto;
  planoFabrico: PlanoFabrico;
  operacoesEscolhidas: Array<Operacao>;
  operacoes: Operacao;
  produto: Produto;

  constructor(private opService: OperacaoService, private service: ProdutoService) { }

  ngOnInit() {
    this.getOperacoes();
  }

  getOperacoes(): void {
    this.opService.getIds().subscribe(data => {
      this.operacoes = data;
    })
  }

  postProduto() {
    this.planoFabrico = new PlanoFabrico(this.operacoesEscolhidas);
    this.produto = new Produto(this.designacao,this.planoFabrico);
    this.service.post(this.produto).subscribe(
      (res : any) => {
        console.log(res);
      },
      (err : any) => {
        console.log(err);
      }
    );
  }
}
