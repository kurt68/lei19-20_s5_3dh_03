import { Injectable } from '@angular/core';
import { Produto } from '../model/Produto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

  public readonly headers;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }
  getIds(): Observable<any> {
    return this.httpClient.get('https://mdp3hgrupo3.azurewebsites.net/api/Produto', { headers: this.headers });
  }
  post(produto: Produto) {
    return this.httpClient.post('https://mdp3hgrupo3.azurewebsites.net/api/Produto', produto, { headers: this.headers });
  }
}
