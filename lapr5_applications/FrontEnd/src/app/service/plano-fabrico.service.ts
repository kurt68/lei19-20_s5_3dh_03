import { Injectable } from '@angular/core';
import { PlanoFabrico } from '../model/planoFabrico';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlanoFabricoService extends GenericService{

  constructor(httpClient: HttpClient) { super('https://mdp3hgrupo3.azurewebsites.net/PlanoFabrico', httpClient)}

  getPlanosFabrico(): Observable<any> {
    return super.getAll();
  }

  getPlanoFabrico(id: number): Observable<PlanoFabrico> {
    return super.getById(id);
  }

  createPlanoFabrico(planoFabrico: PlanoFabrico): Observable<any> {
    return super.create(planoFabrico);
  }

  updatePlanoFabrico(id: number, planoFabrico: PlanoFabrico): Observable<any> {
    return super.update(id, planoFabrico);
  }

  deletePlanoFabrico(id: number): Observable<any> {
    return super.delete(id);
  }
}
