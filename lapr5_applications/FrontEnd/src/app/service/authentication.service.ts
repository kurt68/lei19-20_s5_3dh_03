import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public isLoggedIn : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());

  public isClient : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAClient());

  private readonly AUTH_URL : string = "https://cenas-fixes-kurt.herokuapp.com/users";
  
  constructor(private httpClient: HttpClient) { }

  UtilizadorSignUp(nome: string, username: string, email : string, password: string) : Observable<any> {
    console.log(email);
    return this.httpClient.post(`${this.AUTH_URL}/signup`, {name: nome, username: username, email: email, password : password});
  }

  UtilizadorLogin(username: string, password : string) : Observable<any> {
    return this.httpClient.post(`${this.AUTH_URL}/login`, {username: username, password: password});
  }

  loginSucceeded(data) : void {
    localStorage.setItem("token", data.x.access_token);
    console.log(data);
    let token_info = this.getDecodedAccessToken(data.x.access_token);
    localStorage.setItem("expiryDate", token_info.exp);
    localStorage.setItem("role", token_info.role);
    localStorage.setItem("username", token_info.username);
    this.isLoggedIn.next(this.isAuthenticated());
    this.isClient.next(this.isAClient());
  }

   isAuthenticated() : boolean {
    let token = localStorage.getItem("token");
    let expiryDate = localStorage.getItem("expiryDate");
    if(token != null && expiryDate != null) {
      return new Date().getTime()/1000 < +expiryDate;
    }
    return false;
  }

  isAClient() : boolean {
    return localStorage.getItem("role") === "Cliente";
  }

  setClient(value : boolean) {
    localStorage.setItem("isClient", `${value}`);
    this.isClient.next(this.isAClient());
  }

  getToken() : string {
    return localStorage.getItem("token");
  }

  getLoggedInUsername() : string {
    return localStorage.getItem("username");
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiryDate');
    localStorage.removeItem('username');
    localStorage.removeItem('role');
    this.isLoggedIn.next(this.isAuthenticated());
  }

  private getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }
}