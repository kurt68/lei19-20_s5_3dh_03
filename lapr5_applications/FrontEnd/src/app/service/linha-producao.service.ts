import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LinhaProducao } from '../model/linhaProducao';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LinhaProducaoService {

  public readonly headers;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  getIds(): Observable<any> {
    return this.httpClient.get<any>('https://mdf3hgrupo3.azurewebsites.net/api/LinhaProducao', { headers: this.headers });
  }

  postLinhaProducao(linhaProducao: LinhaProducao) {
    console.log(linhaProducao);
    return this.httpClient.post('https://mdf3hgrupo3.azurewebsites.net/api/LinhaProducao', linhaProducao,  { headers: this.headers });
  }
}
