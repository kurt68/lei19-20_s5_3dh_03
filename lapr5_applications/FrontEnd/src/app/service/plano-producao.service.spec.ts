import { TestBed } from '@angular/core/testing';

import { PlanoProducaoService } from './plano-producao.service';

describe('PlanoProducaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlanoProducaoService = TestBed.get(PlanoProducaoService);
    expect(service).toBeTruthy();
  });
});
