import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilizador } from '../model/utilizador';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly AUTH_URL : string = "https://cenas-fixes-kurt.herokuapp.com/users";

  public readonly headers;

  constructor(private httpClient: HttpClient, private autenticacao: AuthenticationService) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.autenticacao.getToken()}`,
      'Content-Type': 'application/json',
    });
  }
  getUser(): Observable<any>{
    return this.httpClient.get<any>(`${this.AUTH_URL}/consultar/cliente`, { headers: this.headers });
  }

  getUsers(): Observable<any>{
    return this.httpClient.get<any>(`${this.AUTH_URL}/consultar`, { headers: this.headers });
  }
 

  putUser(username, email) {
    return this.httpClient.put(`${this.AUTH_URL}/alterar`,{username: username, email: email}, { headers: this.headers })
  }

  alterarPrioridade(username, pri) {
    return this.httpClient.put(`${this.AUTH_URL}/alterarPrioridade`,{username: username,pri: pri}, { headers: this.headers })
  }
}
