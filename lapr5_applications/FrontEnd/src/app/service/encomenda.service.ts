import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Encomenda } from '../model/encomenda';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import { Produto } from '../model/produto';


@Injectable({
  providedIn: 'root'
})
export class EncomendaService {

  private readonly AUTH_URL: string = "https://cenas-fixes-kurt.herokuapp.com/encomendas";

  public readonly headers;

  constructor(private httpClient: HttpClient, private autenticacao: AuthenticationService) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.autenticacao.getToken()}`,
      'Content-Type': 'application/json',
    });
  }

  getProdutoInfo(produto: Produto): Observable<any> {
    return this.httpClient.get<any>(`${this.AUTH_URL}/encomenda/produtoInfo/` + produto.designacao, { headers: this.headers });
  }

  postEncomenda(encomenda: Encomenda) {
    console.log(encomenda);
    return this.httpClient.post(`${this.AUTH_URL}/encomenda`, encomenda, { headers: this.headers })
  }

  getEncomendas(): Observable<any> {
    return this.httpClient.get<any>(`${this.AUTH_URL}/encomenda`, { headers: this.headers });
  }

  putEncomenda(estado: string, produto: Produto, quantidade: number, morada: string, id) {
    return this.httpClient.put(`${this.AUTH_URL}/encomenda/` + id, { estado: estado, produto: produto, quantidade: quantidade, morada: morada }, { headers: this.headers })
  }

  cancelarEncomenda(id: string) {
    console.log(id)
    return this.httpClient.put(`${this.AUTH_URL}/encomenda/cancelar/` + id, {}, { headers: this.headers });
  }

  getEncomendasParaProducao(): Observable<any> {
    return this.httpClient.get<any>(`${this.AUTH_URL}/encomendasProduzir`, { headers: this.headers });
  }

}
