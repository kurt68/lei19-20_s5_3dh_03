import { Injectable } from '@angular/core';
import { Operacao } from '../model/operacao';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OperacaoService  {

  public readonly headers;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  getIds(): Observable<any> {
    return this.httpClient.get<any>('https://mdf3hgrupo3.azurewebsites.net/api/Operacao', { headers: this.headers });
  }

  postOperacao(operacao: Operacao) {
    return this.httpClient.post('https://mdf3hgrupo3.azurewebsites.net/api/Operacao', operacao, { headers: this.headers })
  }

}
