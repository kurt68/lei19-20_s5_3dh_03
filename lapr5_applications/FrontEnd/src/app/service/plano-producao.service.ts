import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Encomenda } from '../model/encomenda';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import { Produto } from '../model/produto';


@Injectable({
  providedIn: 'root'
})
export class PlanoProducaoService {

  private readonly AUTH_URL: string = "https://cenas-fixes-kurt.herokuapp.com/PlanoProducao";

  public readonly headers;

  constructor(private httpClient: HttpClient, private autenticacao: AuthenticationService) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.autenticacao.getToken()}`,
      'Content-Type': 'application/json',
    });
  }

  getPlanoProducao(): Observable<any> {
    return this.httpClient.get<any>(`${this.AUTH_URL}/Plano` , { headers: this.headers });
  }

  putPlanoProducao(encomendas: Array<any>) {
    return this.httpClient.put(`${this.AUTH_URL}/plano`, { encomendas }, { headers: this.headers })
  }

}
