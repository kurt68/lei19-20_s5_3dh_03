import { Injectable } from '@angular/core';
import { TipoMaquina } from '../model/tipoMaquina';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericService } from './generic.service';
import { Observable } from 'rxjs';
import { Operacao } from '../model/operacao';
import { ParseSpan } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class TipoMaquinaService{

  public readonly headers;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  getIds(): Observable<any> {
    return this.httpClient.get<any>('https://mdf3hgrupo3.azurewebsites.net/api/TipoMaquina', { headers: this.headers });
  }

  postTipoMaquina(tipoMaquina: TipoMaquina) {
    return this.httpClient.post('https://mdf3hgrupo3.azurewebsites.net/api/TipoMaquina', tipoMaquina, { headers: this.headers })
  }

  putTipoMaquina(tipoMaquina: TipoMaquina, nome : string) {
    return this.httpClient.put('https://mdf3hgrupo3.azurewebsites.net/api/TipoMaquina/' + nome, tipoMaquina, { headers: this.headers })
  }

  getMaquinas(designacao: string):  Observable<any> {
    return this.httpClient.get('https://mdf3hgrupo3.azurewebsites.net/api/TipoMaquina/' + designacao + "/Maquinas", { headers: this.headers })
  }

}
