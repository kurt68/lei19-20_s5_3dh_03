import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Maquina } from '../model/maquina';

@Injectable({
  providedIn: 'root'
})
export class MaquinaService{

  public readonly headers;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  getIds(): Observable<any> {
    return this.httpClient.get<any>('https://mdf3hgrupo3.azurewebsites.net/api/Maquina', { headers: this.headers });
  }

  postMaquina(Maquina: Maquina) {
    return this.httpClient.post('https://mdf3hgrupo3.azurewebsites.net/api/Maquina', Maquina, { headers: this.headers })
  }

  putMaquina(Maquina: Maquina, nome: string) {
    return this.httpClient.put('https://mdf3hgrupo3.azurewebsites.net/api/Maquina/' + nome + '/nome', Maquina, { headers: this.headers })
  }

  alterarEstado(Maquina: Maquina, nome: string) {
    console.log("tazzz")
    return this.httpClient.put('https://mdf3hgrupo3.azurewebsites.net/api/Maquina/' + nome + '/estado', Maquina, {headers: this.headers })
  }
}
