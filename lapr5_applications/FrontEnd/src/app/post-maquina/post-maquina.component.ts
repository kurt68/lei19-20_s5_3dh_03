import { Component, OnInit } from '@angular/core';
import { TipoMaquina } from '../model/tipoMaquina';
import { MaquinaService } from '../service/maquina.service';
import { TipoMaquinaService } from '../service/tipo-maquina.service';
import { Maquina } from '../model/maquina';

@Component({
  selector: 'app-post-maquina',
  templateUrl: './post-maquina.component.html',
  styleUrls: ['./post-maquina.component.css']
})
export class PostMaquinaComponent implements OnInit {

  listaTipoMaquina: Array<TipoMaquina>;
  designacao: string;
  tiposMaquinaEscolhidos: TipoMaquina;
  maquina: Maquina;

  
  constructor(private mService: MaquinaService, private tmService : TipoMaquinaService) { }

  ngOnInit() {
    this.getTiposMaquinas();
  }
  
  getTiposMaquinas(): void{
    this.tmService.getIds().subscribe(data => {
      this.listaTipoMaquina = data;
    })
  }

  postMaquina(){
    this.maquina = new Maquina(this.designacao,this.tiposMaquinaEscolhidos, "ATIVADA");
    console.log(this.maquina);
    this.mService.postMaquina(this.maquina).subscribe(
      (res : any) => {
        console.log(res);
      },
      (err : any) => {
        console.log(err);
      }
    );
  }

}
