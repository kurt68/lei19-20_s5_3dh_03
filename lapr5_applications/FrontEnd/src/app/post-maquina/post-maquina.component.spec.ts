import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostMaquinaComponent } from './post-maquina.component';

describe('PostMaquinaComponent', () => {
  let component: PostMaquinaComponent;
  let fixture: ComponentFixture<PostMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
