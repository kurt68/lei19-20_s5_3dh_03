import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostEncomendaComponent } from './post-encomenda.component';

describe('PostEncomendaComponent', () => {
  let component: PostEncomendaComponent;
  let fixture: ComponentFixture<PostEncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostEncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostEncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
