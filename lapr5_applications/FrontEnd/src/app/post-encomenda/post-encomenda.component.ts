import { Component, OnInit } from '@angular/core';
import { Produto } from '../model/produto';
import { Encomenda } from '../model/encomenda';
import { AuthenticationService } from '../service/authentication.service';
import { EncomendaService } from '../service/encomenda.service';
import { ProdutoService } from '../service/produto.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-post-encomenda',
  templateUrl: './post-encomenda.component.html',
  styleUrls: ['./post-encomenda.component.css']
})
export class PostEncomendaComponent implements OnInit {

  codigo: string;
  morada: string;
  username: string;
  quantidade: number;
  produto: Produto;
  produtos: Array<any>;
  encomenda:Encomenda;
  token: string;
  estado: string;
  flag: boolean;
  nrEncome: Number;
  nrQuantidade: Number;
  produtoInfo: any;
  duracao : Number;

  constructor(private autenticacao: AuthenticationService, private encomendaService: EncomendaService,private produtoService: ProdutoService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.username = this.autenticacao.getLoggedInUsername();
    this.getProdutos();

  }

  getProdutos(): void {
    this.produtoService.getIds().subscribe(data => {
      this.produtos = data;
      for (let index = 0; index < this.produtos.length; index++) {
        this.duracao = 0;
        const element = this.produtos[index];
        for (let index = 0; index < element.planoFabrico.operacoes.length; index++) {
          this.duracao = this.duracao + element.planoFabrico.operacoes[index].duracao;
        }
        this.produtos[index].duracao = this.duracao;
      }
      
    })
  }

  getProdutoInfo(): void {
    this.encomendaService.getProdutoInfo(this.produto).subscribe(data => {this.nrQuantidade = data[0], this.nrEncome = data[1]});
    this.flag = true;
  }

  postEncomendas(): void {
    this.encomenda = new Encomenda(this.codigo, this.username, this.produto, this.quantidade, this.morada);
    console.log(this.encomenda);
    this.encomendaService.postEncomenda(this.encomenda).subscribe(
      (res : any) => {
        console.log(res);
        this._snackBar.open("Criada com sucesso!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("Encomenda não pode ser criada, introduza dados validos!", "", {
          duration: 4500,
        });
      }
    );
  }

}
