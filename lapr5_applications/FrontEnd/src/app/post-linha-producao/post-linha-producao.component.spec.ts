import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostLinhaProducaoComponent } from './post-linha-producao.component';

describe('PostLinhaProducaoComponent', () => {
  let component: PostLinhaProducaoComponent;
  let fixture: ComponentFixture<PostLinhaProducaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostLinhaProducaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostLinhaProducaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
