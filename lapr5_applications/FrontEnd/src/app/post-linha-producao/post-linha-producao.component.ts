import { Component, OnInit } from '@angular/core';
import { Maquina } from '../model/maquina';
import { MaquinaService } from '../service/maquina.service';
import { LinhaProducaoService } from '../service/linha-producao.service';
import { LinhaProducao } from '../model/linhaProducao';


@Component({
  selector: 'app-post-linha-producao',
  templateUrl: './post-linha-producao.component.html',
  styleUrls: ['./post-linha-producao.component.css']
})
export class PostLinhaProducaoComponent implements OnInit {

  designacao: string;
  maquinasEscolhidas: Array<Maquina>;
  maquinas: Array<Maquina>;
  linha: LinhaProducao;

  constructor(private maquinaService: MaquinaService, private linhaService: LinhaProducaoService) { }

  ngOnInit() {
    this.getMaquinas();
  }

  getMaquinas() {
    this.maquinaService.getIds().subscribe(data => {
      this.maquinas = data;
    });
  }

  postLinhaProducao() {
    this.linha = new LinhaProducao(this.designacao, this.maquinasEscolhidas);
    console.log(this.linha);
    this.linhaService.postLinhaProducao(this.linha).subscribe(
      (res: any) => {
        console.log(res);
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
}
