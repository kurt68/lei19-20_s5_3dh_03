import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetProdutoComponent } from './get-produto.component';

describe('GetProdutoComponent', () => {
  let component: GetProdutoComponent;
  let fixture: ComponentFixture<GetProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetProdutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
