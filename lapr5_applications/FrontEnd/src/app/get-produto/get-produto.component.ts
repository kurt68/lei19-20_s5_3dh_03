import { Component, OnInit } from '@angular/core';
import { Produto } from '../model/produto';
import { ProdutoService } from '../service/produto.service';
import { Router } from '@angular/router';
import { PlanoFabrico } from '../Model/PlanoFabrico';
import { Operacao } from '../model/operacao';

@Component({
  selector: 'app-get-produto',
  templateUrl: './get-produto.component.html',
  styleUrls: ['./get-produto.component.css']
})
export class GetProdutoComponent implements OnInit {

  selected: Produto;
  produtos: Array<string>;
  flag: boolean = false;
  operacoes: Array<Operacao>;

  constructor(private router: Router, private service: ProdutoService) { }

  ngOnInit() {
    console.log("lol");
    this.getProdutos();
  }
  getProdutos(): void {
    this.service.getIds().subscribe(data => {
      this.produtos = data;
    })
  }

  getPlano(): void {
    this.operacoes = this.selected.planoFabrico.operacoes;
    this.flag = true;
  }

}
