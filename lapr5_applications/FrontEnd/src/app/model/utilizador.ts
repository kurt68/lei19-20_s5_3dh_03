export class Utilizador {
    username: string;
    email: string;
    name: string;
    prioridade: number;
    
    constructor(username:string,email:string,name: string, prioridade:number) {
        this.username = username;
        this.email = email;
        this.name = name;
        this.prioridade = prioridade;
    }
}