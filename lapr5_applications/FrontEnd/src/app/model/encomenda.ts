import { Produto } from './produto';

export class Encomenda {
    codigo: string;
    username: string;
    produto: Produto;
    quantidade: number;
    morada: string;
    
    constructor(codigo:string,username:string,produto: Produto,quantidade: number, morada:string) {
    
        this.codigo = codigo;
        this.username = username;
        this.produto = produto;
        this.quantidade = quantidade;
        this.morada = morada;
    }
}