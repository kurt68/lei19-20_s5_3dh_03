import { Maquina } from './maquina';

export class LinhaProducao {
    designacao: string;
    sequenciaMaquinas: Array<Maquina>;

    constructor(designacao: string, maquinas: Array<Maquina>) {
        this.designacao = designacao;
        this.sequenciaMaquinas = maquinas;
    }
}