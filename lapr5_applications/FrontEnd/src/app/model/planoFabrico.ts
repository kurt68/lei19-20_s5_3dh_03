import { Operacao } from './operacao';

export class PlanoFabrico {
    operacoes: Operacao[];

    constructor(operacoes: Operacao[]) {
        this.operacoes = operacoes;
    }
}