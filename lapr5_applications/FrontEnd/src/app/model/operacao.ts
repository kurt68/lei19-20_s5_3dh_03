export class Operacao {
    designacao: string;
    ferramenta: string;
    duracao: Float32Array;

    constructor(designacao: string, ferramenta: string, duracao: Float32Array) {
      this.designacao = designacao;
      this.ferramenta = ferramenta;
      this.duracao = duracao;
  }
}  