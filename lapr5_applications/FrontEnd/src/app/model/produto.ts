import { PlanoFabrico } from '../Model/planoFabrico'

export class Produto{
    designacao: string;
    planoFabrico: PlanoFabrico;

    constructor(designacao: string, planoFabrico: PlanoFabrico){
        this.designacao = designacao;
        this.planoFabrico = planoFabrico;
    }
}