import { Operacao } from './operacao';

export class TipoMaquina{
    designacao: string;
    operacoes: Operacao[];

    constructor(designacao: string, operacoes: Operacao[]){
        this.designacao = designacao;
        this.operacoes = operacoes;
    }
}