import { TipoMaquina } from './tipoMaquina';

export class Maquina{

    nome: string;
    tipo: TipoMaquina;
    estado: string;

    constructor(nome: string, tipo: TipoMaquina, estado: string){
        this.nome = nome;
        this.tipo = tipo;
        this.estado = estado;
    }
}