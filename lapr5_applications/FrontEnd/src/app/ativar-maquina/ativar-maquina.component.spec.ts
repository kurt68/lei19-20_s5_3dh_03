import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivarMaquinaComponent } from './ativar-maquina.component';

describe('AtivarMaquinaComponent', () => {
  let component: AtivarMaquinaComponent;
  let fixture: ComponentFixture<AtivarMaquinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivarMaquinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivarMaquinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
