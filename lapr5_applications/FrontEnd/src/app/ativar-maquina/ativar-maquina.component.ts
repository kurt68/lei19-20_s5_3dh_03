import { Component, OnInit } from '@angular/core';
import { Maquina } from '../model/maquina';
import { Router } from '@angular/router';
import { MaquinaService } from '../service/maquina.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-ativar-maquina',
  templateUrl: './ativar-maquina.component.html',
  styleUrls: ['./ativar-maquina.component.css']
})
export class AtivarMaquinaComponent implements OnInit {

  flag: boolean;
  maquinas: Array<Maquina>;
  selected: Maquina;
  ativada: string = "ATIVADA";
  desativada: string = "DESATIVADA";

  constructor(private router: Router, private service: MaquinaService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getMaquinas();
  }
  getMaquinas(): void {
    this.service.getIds().subscribe(data => {
      this.maquinas = data;
    })
  }

  getMaquina() {
    this.flag = true;
  }

  ativarMaquina() {
    this.selected.estado = this.ativada;
    this.service.alterarEstado(this.selected, this.selected.nome).subscribe(
      (res : any) => {
        console.log(res);
        this._snackBar.open("Maquina Ativada!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("Erro!", "", {
          duration: 4500,
        });
      }
    );;;

  }

  desativarMaquina() {
    this.selected.estado = this.desativada;
    this.service.alterarEstado(this.selected, this.selected.nome).subscribe(
      (res : any) => {
        console.log(res);
        this._snackBar.open("Maquina Desativada!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("Erro!", "", {
          duration: 4500,
        });
      }
    );;;;
  }

}
