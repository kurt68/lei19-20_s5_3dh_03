import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetOperacaoComponent } from './get-operacao.component';

describe('GetOperacaoComponent', () => {
  let component: GetOperacaoComponent;
  let fixture: ComponentFixture<GetOperacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetOperacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetOperacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
