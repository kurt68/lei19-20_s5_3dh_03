import { Component, OnInit } from '@angular/core';
import { Operacao } from '../model/operacao';
import { OperacaoService } from '../service/operacao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-get-operacao',
  templateUrl: './get-operacao.component.html',
  styleUrls: ['./get-operacao.component.css']
})
export class GetOperacaoComponent implements OnInit {

  operacoes: Array<Operacao>;
  selected: Operacao;
  flag: boolean = false;



  constructor(private router: Router, private service: OperacaoService) {


  }

  ngOnInit() {
    console.log("x");
    this.getOperacoes();
  }

  getOperacoes(): void {
    console.log("x");

    this.service.getIds().subscribe(data => {
      this.operacoes = data;
    })
   
  }

  getOperacao():void {
    this.flag = true;
  }

}
