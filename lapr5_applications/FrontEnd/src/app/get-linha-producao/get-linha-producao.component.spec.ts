import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetLinhaProducaoComponent } from './get-linha-producao.component';

describe('GetLinhaProducaoComponent', () => {
  let component: GetLinhaProducaoComponent;
  let fixture: ComponentFixture<GetLinhaProducaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetLinhaProducaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetLinhaProducaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
