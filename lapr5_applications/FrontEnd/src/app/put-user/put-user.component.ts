import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Utilizador } from '../model/utilizador';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-put-user',
  templateUrl: './put-user.component.html',
  styleUrls: ['./put-user.component.css']
})
export class PutUserComponent implements OnInit {

  user: Utilizador;
  username: string;
  email: string;
  
  constructor(private service: UserService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void{
    this.service.getUser().subscribe(data => {
      this.user = data.user;
    })
  }

  putUser(){
    this.service.putUser(this.username, this.email).subscribe(
      (res : any) => {
        console.log(res);
        this._snackBar.open("Alterações efectuadas!", "", {
          duration: 3500,
        });
      },
      (err : any) => {
        console.log(err);
        this._snackBar.open("Alterações não efectuadas!", "", {
          duration: 4500,
        });
      }
    );
  }
}
