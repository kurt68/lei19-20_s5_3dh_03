import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  page = {
    title: 'My Own Cutlery',
    subtitle: 'Menu',
  };

  eCliente: boolean;
  estaLogin: boolean;

  constructor(private router: Router, private autenticacao: AuthenticationService) { 
    this.cliente();
    this.login();
  }

  ngOnInit() {
  }

  cliente(): void{
    this.autenticacao.isClient.subscribe(check =>{
      this.eCliente = check;
    });
  }

 login():void{
   this.autenticacao.isLoggedIn.subscribe(check =>{
    this.estaLogin = check;
   });
 }

 sair():void{
   this.autenticacao.logout();
   this.router.navigateByUrl(''); 
 }

  getOperacoes() {
    this.router.navigateByUrl('/operacao/get'); 

  }
  postOperacoes() {
    this.router.navigateByUrl('/operacao/post');
  }

  getMaquinas(){
    this.router.navigateByUrl('/maquina/get');
  }

  getTiposMaquinas(){
    this.router.navigateByUrl('/tipoMaquina/get');
  }

  postTipoMaquinas(){
    this.router.navigateByUrl('/tipoMaquina/post');
  }

  putTipoMaquinas(){
    this.router.navigateByUrl('/tipoMaquina/put');
  }

  getProdutos(){
    this.router.navigateByUrl('/produto/get');
  }
  postProdutos() {
    this.router.navigateByUrl('/produto/post');
  }

  getLinhasProducao(){
    this.router.navigateByUrl('/linhaProducao/get');
  }
    
  postMaquinas(){
    this.router.navigateByUrl('/maquina/post');
  }

  putMaquinas(){
    this.router.navigateByUrl('/maquina/put');
  }

  postLinhasProducao(){
    this.router.navigateByUrl('/linhaProducao/post');
  }

  criarEncomenda(){
    this.router.navigateByUrl('/encomenda/post');
  }

  consultarEncomendas() {
    this.router.navigateByUrl('encomendaAdmin/get');
  }

  editarEncomenda() {
    this.router.navigateByUrl('encomenda/put');
  }

  ordenarEncomendas(){
    this.router.navigateByUrl('ordenarEncomendas');
  }

  consultarEncomendasCliente(){
    this.router.navigateByUrl('/encomenda/get');
  }

  cancelarEncomenda() {
    this.router.navigateByUrl('encomenda/cancelar');
  }

  ativarMaquina() {
    this.router.navigateByUrl('ativarMaquina');
  }

  consltarDados(){
    this.router.navigateByUrl('/user/get');
  }

  consultarClientes(){
    this.router.navigateByUrl('user/getAll');
  }

  editarDados(){
    this.router.navigateByUrl('/user/put');
  }

  home() {
    this.router.navigateByUrl('');
  }

}
